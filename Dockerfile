# Create base node image and install git and turbo and read scope arg
FROM node:alpine AS base
RUN apk update && npm i -g turbo && apk add curl bash && curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin
ARG SCOPE
ENV SCOPE=${SCOPE}

# Prune the workspace for the app in given scope
FROM base as pruner
WORKDIR /app
COPY . .
RUN turbo prune --scope=${SCOPE} --docker

# Add pruned lockfile and package.json's of the pruned subworkspace
FROM pruner AS builder
WORKDIR /app
COPY --from=pruner /app/out/json/ .
COPY --from=pruner /app/out/yarn.lock ./yarn.lock

# Install the dependencies and build the next project
RUN yarn install --frozen-lockfile
ENV NODE_ENV=production
RUN yarn run build --scope=${SCOPE} --include-dependencies --no-deps

# Clean node module folder for production and clear next cache
RUN npm prune --production --no-audit
RUN /usr/local/bin/node-prune
RUN rm -rf /app/apps/${SCOPE}/.next/cache

# Copy relevant files from builder and start the next server
FROM gcr.io/distroless/nodejs
WORKDIR /app
ARG SCOPE
ENV SCOPE=${SCOPE}
ENV NODE_ENV=production
COPY --from=builder /app/node_modules/ ./node_modules/
COPY --from=builder /app/apps/${SCOPE}/.next/ ./.next/
COPY --from=builder /app/apps/${SCOPE}/package.json ./package.json
COPY --from=builder /app/apps/${SCOPE}/next.config.js ./next.config.js
COPY --from=builder /app/apps/${SCOPE}/next-i18next.config.js ./next.config.js
COPY --from=builder /app/apps/${SCOPE}/public/ ./public
EXPOSE 3000
CMD ["node_modules/.bin/next", "start"]

