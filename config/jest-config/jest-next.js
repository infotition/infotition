const nextJest = require('next/jest');

module.exports = (dir) => {
  const createJestConfig = nextJest({ dir });

  const customJestConfig = {
    setupFilesAfterEnv: [`${dir}/jest.setup.ts`],
    testEnvironment: 'jest-environment-jsdom',
    moduleNameMapper: {
      '@Elements/(.*)/(.*)$': `${dir}/app/Components/Elements/$1/$2`,
      '@Layouts/(.*)/(.*)$': `${dir}/app/Components/Layouts/$1/$2`,
      '@Modules/(.*)/(.*)$': `${dir}/app/Components/Modules/$1/$2`,
      '@Templates/(.*)/(.*)$': `${dir}/app/Components/Templates/$1/$2`,
      '@Helpers/(.*)/(.*)$': `${dir}/app/Components/Helpers/$1/$2`,
      '@Context/(.*)$': `${dir}/app/Context/$1`,
      '@Hooks/(.*)$': `${dir}/app/Hooks/$1`,
      '@Utils/(.*)$': `${dir}/app/Utils/$1`,
      '@Constants/(.*)$': `${dir}/app/Constants/$1`,
      '@Models/(.*)$': `${dir}/app/Models/$1`,
      '@Controllers/(.*)$': `${dir}/app/Controllers/$1`,
    },
  };

  return createJestConfig(customJestConfig);
};
