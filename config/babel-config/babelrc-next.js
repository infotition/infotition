module.exports = {
  presets: ['next/babel', '@babel/preset-typescript'],
  plugins: [
    'babel-plugin-transform-typescript-metadata',
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    'babel-plugin-parameter-decorator',
    [
      'module-resolver',
      {
        root: ['./'],
        alias: {
          '@Elements': './app/Components/Elements',
          '@Layouts': './app/Components/Layouts',
          '@Modules': './app/Components/Modules',
          '@Templates': './app/Components/Templates',
          '@Helpers': './app/Components/Helpers',
          '@Api/*': ['./app/Ap*'],
          '@Hooks/*': ['./app/Hooks'],
          '@Styles/*': ['./app/Styles'],
          '@Utils/*': ['./app/Utils'],
          '@Context/*': ['./app/Context'],
          '@Lib/*': ['./lib'],
        },
      },
    ],
    [
      'babel-plugin-styled-components',
      {
        ssr: true,
        displayName: true,
      },
    ],
  ],
};
