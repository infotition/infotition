module.exports = {
  darkMode: 'class',
  content: [
    '**/pages/**/*.{js,jsx,ts,tsx}',
    '**/app/Components/**/*.{js,jsx,ts,tsx}',
  ],
  //important: true,
  theme: {
    screens: {
      mobile: '375px',
      tablet: '960px',
      desktop: '1248px',
    },
    colors: {
      transparent: '#00000000',
      neutral: {
        900: '#212134',
        800: '#32324D',
        700: '#4A4A6A',
        600: '#666687',
        500: '#8E8EA9',
        400: '#A5A5BA',
        300: '#C0C0CF',
        200: '#DCDCE4',
        150: '#EAEAEF',
        100: '#F6F6F9',
        0: '#FFFFFF',
      },
      primary: {
        700: '#5927E5',
        600: '#7A52EA',
        500: '#9B7DEF',
        200: '#BDA9F5',
        100: '#DED4FA',
      },
      secondary: {
        700: '#FED3D4',
        600: '#FEDCDD',
        500: '#FEE5E5',
        200: '#FFEDEE',
        100: '#FFF6F6',
      },
      accent: {
        700: '#65BAA9',
        600: '#84C8BA',
        500: '#A3D6CB',
        200: '#C1E3DD',
        100: '#E0F1EE',
      },
      success: {
        700: '#66CF80',
        600: '#85D999',
        500: '#A3E2B3',
        200: '#D1F1D9',
        100: '#F0FAF2',
      },
      warning: {
        700: '#FFD667',
        600: '#FFDE85',
        500: '#FFE6A4',
        200: '#FFF3D1',
        100: '#FFF7E1',
      },
      danger: {
        700: '#E00061',
        600: '#E63381',
        500: '#EC66A0',
        200: '#EC66A0',
        100: '#F9CCDF',
      },
    },
    fontFamily: {
      poppins: ['Poppins', 'sans-serif'],
      inter: ['Inter', 'sans-serif'],
    },
    fontSize: {
      header1: ['60px', { lineHeight: '72px', letterSpacing: '0.01em' }],
      header1m: ['45px', { lineHeight: '56px', letterSpacing: '0.01em' }],

      header2: ['50px', { lineHeight: '64px', letterSpacing: '0.01em' }],
      header2m: ['31px', { lineHeight: '48px', letterSpacing: '0.01em' }],

      header3: ['40px', { lineHeight: '56px', letterSpacing: '0.01em' }],
      header3m: ['25px', { lineHeight: '40px', letterSpacing: '0.01em' }],

      header4: ['33px', { lineHeight: '40px', letterSpacing: '0.01em' }],
      header4m: ['21px', { lineHeight: '34px', letterSpacing: '0.01em' }],

      header5: ['24px', { lineHeight: '32px', letterSpacing: '0.01em' }],
      header5m: ['17px', { lineHeight: '32px', letterSpacing: '0.01em' }],

      body1: ['24px', { lineHeight: '38px', letterSpacing: '0.01em' }],
      body2: ['20px', { lineHeight: '34px', letterSpacing: '0.01em' }],
      body3: ['17px', { lineHeight: '28px', letterSpacing: '0.01em' }],
      body4: ['15px', { lineHeight: '24px', letterSpacing: '0.01em' }],
      body5: ['13px', { lineHeight: '22px', letterSpacing: '0.01em' }],
    },
    extend: {
      animation: {
        'bounce-right': 'bounce-right 1s ease-in-out infinite',
        pop: 'pop .6s ease',
        'fade-pop': 'fade-pop .4s ease-in-out',
      },
      keyframes: {
        'bounce-right': {
          '0%': { transform: 'translateX(0)' },
          '50%': { transform: 'translateX(7px)' },
        },
        pop: {
          '50%': { transform: 'scale(1.2)' },
        },
        'fade-pop': {
          '50%': { opacity: '0.2', transform: 'scale(1.3)' },
          '100%': { opacity: '0', transform: 'scale(1.7)' },
        },
      },
      container: {
        padding: '1rem',
      },
      height: {
        screen: 'calc(var(--inner-vh, 1vh) * 100)',
      },
      minHeight: {
        screen: 'calc(var(--inner-vh, 1vh) * 100)',
      },
    },
  },
  plugins: [],
};
