import { useEffect, useRef, useState } from 'react';
import debounce from 'lodash.debounce';

/**
 * Hook to determine the size of the window.
 *
 * @returns An object containing the window dimensions.
 */
const useWindowSize = (delay: number) => {
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  const handleResize = useRef(
    debounce(async () => {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }, delay)
  ).current;

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    handleResize();

    return () => {
      handleResize.cancel();
      window.removeEventListener('resize', handleResize);
    };
  }, [handleResize]);

  return windowSize;
};

export default useWindowSize;
