import { createContext, useContext, FunctionComponent } from 'react';

type State = { calcHeight: (el: any) => void };

const DropdownStateContext = createContext<State | null>(null);

/**
 * The Provider for the Dropdown context.
 */
const DropdownProvider: FunctionComponent<State> = ({
  children,
  calcHeight,
}) => {
  return (
    <DropdownStateContext.Provider value={{ calcHeight }}>
      {children}
    </DropdownStateContext.Provider>
  );
};

/**
 * Wraps around the use context hook for the Dropdown context.
 *
 * @throws useDropdown must be used within a DropdownProvider
 *
 * @returns The current context of the Dropdown.
 */
const useDropdown = () => {
  const context = useContext(DropdownStateContext);

  if (!context)
    throw new Error('useDropdown must be used within a DropdownProvider');

  return context;
};

export { DropdownProvider, useDropdown };
