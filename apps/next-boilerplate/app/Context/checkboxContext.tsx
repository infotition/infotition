import { createContext, useContext, FunctionComponent } from 'react';

type State = { disabled: boolean };

const CheckboxStateContext = createContext<State | null>(null);

/**
 * The Provider for the checkbox context, which provides
 * values for checkbox compound components.
 *
 * @param disabled  Whether the input field should be disabled or not.
 */
const CheckboxProvider: FunctionComponent<State> = ({ children, disabled }) => {
  return (
    <CheckboxStateContext.Provider value={{ disabled }}>
      {children}
    </CheckboxStateContext.Provider>
  );
};

/**
 * Wraps around the use context hook for the checkbox context.
 *
 * @throws useCheckbox must be used within a CheckboxProvider
 *
 * @returns The current context of the checkbox.
 */
const useCheckbox = () => {
  const context = useContext(CheckboxStateContext);

  if (!context)
    throw new Error('useCheckbox must be used within a CheckboxProvider');

  return context;
};

export { CheckboxProvider, useCheckbox };
