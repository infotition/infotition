import {
  User,
  Provider,
  UserCredentials,
  AuthChangeEvent,
  Session,
  ApiError,
} from '@supabase/supabase-js';
import { createContext, useEffect, useState, useContext } from 'react';
// import Router from 'next/router';
import toast from 'react-hot-toast';

import { supabase } from '@Lib/initSubabase';
import { FunctionComponent } from 'react';

type State = {
  details: {
    user: User;
    loggedIn: boolean;
    loading: boolean;
    authCookieSet: boolean;
  };
  functions: {
    signUp: (payload: UserCredentials, captchaToken: string) => void;
    signIn: (payload: UserCredentials, captchaToken: string) => void;
    signInWithProvider: (provider: Provider) => Promise<void>;
  };
};

const AuthContext = createContext<Partial<State>>({});

const handleSignUpError = (error: ApiError) => {
  switch (error.status) {
    case 429:
      return toast.error(
        'You must wait 1 minute before you can register again.'
      );
    case 442:
      return toast.error('Invalid captcha. Please try again.');
    default:
      return toast.error('An error occurred, please try again later.');
  }
};

const handleSignInError = (error: ApiError) => {
  switch (error.status) {
    case 400:
      return toast.error('Invalid login credentials.');
    case 442:
      return toast.error('Invalid captcha. Please try again.');
    default:
      return toast.error('An error occurred, please try again later.');
  }
};

const setServerSession = async (event: AuthChangeEvent, session: Session) => {
  await fetch('/api/auth', {
    method: 'POST',
    headers: new Headers({ 'Content-Type': 'application/json' }),
    credentials: 'same-origin',
    body: JSON.stringify({ event, session }),
  });
};

/**
 * The Provider for the auth context, which provides auth functions and values.
 */
export const AuthProvider: FunctionComponent = ({ children }) => {
  const [user, setUser] = useState<User>(null);
  const [loading, setLoading] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);
  const [authCookieSet, setAuthCookieSet] = useState(false);

  /**
   * Sign up an account with email and password.
   *
   * @param payload An object containing email and password of the user.
   */
  const signUp = async (payload: UserCredentials, captchaToken: string) => {
    try {
      setLoading(true);

      const response = await fetch('/api/register', {
        method: 'POST',
        headers: new Headers({ 'Content-Type': 'application/json' }),
        credentials: 'same-origin',
        body: JSON.stringify({ ...payload, captchaToken }),
      });

      const { error } = await response.json();

      if (error) return handleSignUpError(error);

      //Router.push('/register/success');
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  /**
   * Sign in to the account with email and password.
   *
   * @param payload An object containing email and password of the user.
   */
  const signIn = async (payload: UserCredentials, captchaToken: string) => {
    try {
      setLoading(true);
      const data = await supabase.auth.signIn(payload);

      if (data.error) handleSignInError(data.error);
      // else Router.push('/account');
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  /**
   * Sign in with the given auth provider.
   *
   * @param provider The auth provider to sign in with.
   */
  const signInWithProvider = async (provider: Provider) => {
    await supabase.auth.signIn({ provider });
  };

  /**
   * Read the user data in browser context and safe it to the context
   * if a user was found (logged in). If an auth event happens, send
   * it to the api backend.
   */
  useEffect(() => {
    const currUser = supabase.auth.user();

    if (currUser) {
      setUser(currUser);
      setLoggedIn(true);
      setAuthCookieSet(true);
    }

    const { data: authListener } = supabase.auth.onAuthStateChange(
      async (event, session) => {
        console.log(event, session);

        const sessionUser = session?.user || null;

        setAuthCookieSet(false);
        await setServerSession(event, session);
        setAuthCookieSet(true);

        setUser(sessionUser);
        setLoggedIn(sessionUser !== null);
      }
    );

    return () => authListener.unsubscribe();
  }, []);

  const details = { user, loggedIn, loading, authCookieSet };
  const functions = { signUp, signInWithProvider, signIn };

  return (
    <AuthContext.Provider value={{ details, functions }}>
      {children}
    </AuthContext.Provider>
  );
};

/**
 * Wraps around the use context hook for the auth context.
 *
 * @throws useAuth must be used within a AuthProvider
 *
 * @returns The current context of the auth state.
 */
export const useAuth = () => {
  const context = useContext(AuthContext);

  if (context === undefined) {
    throw new Error('useAuth must be used within a AuthProvider');
  }

  return context;
};
