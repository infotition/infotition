export interface IconProps {
  dimension?: number;
  className?: string;
  icon: 'github' | 'facebook' | 'google';
  onClick?: VoidFunction;
}
