import classNames from 'classnames';
import { FunctionComponent } from 'react';

import { icons } from './Icon.constants';
import type { IconProps } from './Icon.types';

/**
 * Generates a icon with provided dimension and type.
 *
 * @param dimension   (25)    The width/height of the icon.
 * @param className   ('')    The styling which should be applied to the component.
 * @param icon                The rendered icon type.
 *
 * @example <Icon icon="google" />
 */
const Icon: FunctionComponent<IconProps> = ({
  dimension = 25,
  className = '',
  icon,
}: IconProps) => {
  const getIconStyle = () => {
    switch (icon) {
      case 'facebook':
        return 'text-[#4285f4]';
      case 'github':
        return 'text-[#191717] dark:text-[#fff]';
      default:
        return '';
    }
  };

  return (
    <svg
      className={classNames(className, getIconStyle(), 'select-none')}
      width={dimension}
      height={dimension}
      fill="currentColor"
      stroke="none"
      viewBox="0 0 24 24"
      focusable="false"
      aria-hidden
    >
      <label id={`icon-${icon}`}>{`${icon} icon`}</label>
      {icons[icon]}
    </svg>
  );
};

export default Icon;
