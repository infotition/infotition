/* eslint-disable @typescript-eslint/no-explicit-any */

import { FunctionComponent } from 'react';

export type IconButtonProps = {
  className?: string;
  ButtonIcon: FunctionComponent<any>;
  onClick?: CallableFunction;
  active?: boolean;
  label?: string;
};
