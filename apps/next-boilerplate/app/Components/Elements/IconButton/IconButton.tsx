import Tippy from '@tippyjs/react';
import classNames from 'classnames';
import { FunctionComponent } from 'react';

import type { IconButtonProps } from './IconButton.types';

/**
 * Rounded icon button component.
 *
 * @param className   ('')      The styling which should be applied to the component.
 * @param ButtonIcon            The icon which should get displayed.
 * @param onClick     (null)    The function which gets invoked after clicking.
 * @param active      (false)   Whether the button should be styled active or not.
 * @param label       ('')      The aria and tooltip label of the button.
 *
 * @example <IconButton ButtonIcon={BellIcon} />
 */
const IconButton: FunctionComponent<IconButtonProps> = ({
  children,
  className = '',
  onClick = null,
  ButtonIcon,
  active = false,
  label = '',
}) => {
  /**
   * Returns the style based on the active prop.
   */
  const getVariantStyles = () => {
    if (active)
      return 'bg-primary-500/30 text-primary-500 dark:text-primary-200';

    return 'bg-neutral-200 dark:bg-neutral-700';
  };

  return (
    <Tippy
      content={label}
      animation="shift-away"
      delay={300}
      arrow={false}
      placement="bottom"
      theme="basic"
    >
      <button
        aria-label={`open ${label}`}
        onClick={() => onClick && onClick()}
        className={classNames(
          className,
          getVariantStyles(),
          'w-9 h-9 rounded-full hover:brightness-90 dark:hover:brightness-125 active:scale-90 transition-all flex items-center justify-center'
        )}
      >
        <div className="w-5 h-5 ">
          <ButtonIcon />
          {children}
        </div>
      </button>
    </Tippy>
  );
};

export default IconButton;
