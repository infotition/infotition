export type AvatarProps = {
  className?: string;
  url?: string;
};
