import classNames from 'classnames';
import { FunctionComponent } from 'react';
import Image from 'next/image';

import type { AvatarProps } from './Avatar.types';

/**
 * Generates a avatar with an image of the provided url. If no url is provided,
 * it defaults to an standard avatar.
 *
 * @param className ('')  he styling which should be applied to the component.
 * @param url       ('')  The url of the avatar image.
 *
 * @example <Avatar url="..."/>
 */
const Avatar: FunctionComponent<AvatarProps> = ({
  className = '',
  url = '',
}) => {
  return (
    <div
      className={classNames(
        className,
        'aspect-square rounded-full relative overflow-hidden w-8 h-8'
      )}
    >
      <Image
        src={url}
        alt="test"
        layout="fill"
        className="pointer-events-none select-none"
      />
    </div>
  );
};

export default Avatar;
