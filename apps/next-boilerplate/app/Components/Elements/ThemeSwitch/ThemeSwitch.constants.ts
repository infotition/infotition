/**
 * Maps a theme switch position to the corresponding tailwind
 * absolute position utility classes.
 */
export const switchPositionMap = new Map<string, string>([
  ['upper_right', 'right-5 top-5'],
  ['upper_left', 'left-5 top-5'],
  ['bottom_right', 'right-5 bottom-5'],
  ['bottom_left', 'left-5 bottom-5'],
]);
