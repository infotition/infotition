import classNames from 'classnames';
import { FunctionComponent } from 'react';

import { InfotitionLogo } from '@Elements/VectorGraphic';
import LinkWrapper from '@Helpers/LinkWrapper';

import type { BrandProps } from './Brand.types';

/**
 * Brand component with infotition logo and brand name.
 *
 * @param className   ('')  The styling which should be applied to the component.
 * @param redirect    ('')  The url which the user should redirect to after clicked.
 *
 * @example <Brand redirect="/" />
 */
const Brand: FunctionComponent<BrandProps> = ({
  className = '',
  redirect = '',
}) => {
  return (
    <div
      className={classNames(
        ' text-primary-700 dark:text-primary-200',
        className
      )}
    >
      <LinkWrapper className="flex gap-2" href={redirect}>
        <InfotitionLogo scale={0.25} />
        <span className="body2 font-semibold select-none">NFOTITION</span>
      </LinkWrapper>
    </div>
  );
};

export default Brand;
