export { InfotitionLogo } from './VectorGraphics/InfotitionLogo';
export { AbstractDotPattern } from './VectorGraphics/AbstractDotPattern';
export { AbstractCirclePattern } from './VectorGraphics/AbstractCirclePattern';
export type { VectorGraphicsProps } from './VectorGraphics/VectorGraphics.types';
