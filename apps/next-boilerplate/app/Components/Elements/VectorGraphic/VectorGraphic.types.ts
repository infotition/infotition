export type VectorGraphicsProps = {
  ariaHidden?: boolean;
  children: React.ReactNode;
  className?: string;
  height: number;
  scale?: number;
  title?: string;
  width: number;
};
