import { memo, FunctionComponent } from 'react';

import type { VectorGraphicsProps } from './VectorGraphic.types';

/**
 * Wraps path tags with an svg tag with a11y attributes and dimension
 * configuration.
 *
 * @param ariaHidden  (false) If screen readers should read out the svg or not.
 * @param className   ('')    The class names to style the svg.
 * @param height              The height of the svg.
 * @param scale       (1)     The scale factor of the svg.
 * @param title       ('')    The title of the svg for screen readers.
 * @param width               The width of the svg.
 */
const VectorGraphic: FunctionComponent<VectorGraphicsProps> = ({
  children,
  ariaHidden = false,
  className = '',
  height,
  scale = 1,
  title = '',
  width,
}) => {
  return (
    <svg
      aria-hidden={ariaHidden}
      className={className}
      fill="currentColor"
      height={height * scale}
      viewBox={`0 0 ${width} ${height}`}
      role="img"
      width={width * scale}
      xmlns="http://www.w3.org/2000/svg"
    >
      <title>{title}</title>
      {children}
    </svg>
  );
};

export default memo(VectorGraphic);
