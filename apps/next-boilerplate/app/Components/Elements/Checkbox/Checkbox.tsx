import classNames from 'classnames';
import { FunctionComponent, useState } from 'react';

import type {
  CheckboxProps,
  TickProps,
  LabelProps,
  InputProps,
} from './Checkbox.types';
import styles from './Checkbox.module.scss';
import { CheckboxProvider, useCheckbox } from '@Context/checkboxContext';

interface CheckboxComposition {
  Label: FunctionComponent<LabelProps>;
  Input: FunctionComponent<InputProps>;
}

/**
 * Generates a custom tick svg, which can get animated with dash offset.
 *
 * @param checked         Whether the checkbox is checked or not.
 * @param className ('')  The Styling which should be applied to the component.
 *
 * @example <Tick checked />
 */
const Tick: FunctionComponent<TickProps> = ({ checked, className = '' }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 12 9"
      fill="none"
      strokeLinecap="round"
      stroke="currentColor"
      strokeWidth={'2px'}
      transform="scale(0.6)"
      className={classNames(
        className,
        'text-neutral-0 z-10 pointer-events-none'
      )}
      aria-hidden
    >
      <polyline
        className={checked ? styles.checked : ''}
        strokeDasharray={'16px'}
        strokeDashoffset={'16px'}
        points="1 5 4 8 11 1"
      />
    </svg>
  );
};

/**
 * Generates a custom checkbox.
 *
 * @param className ('')  The Styling which should be applied to the component.
 *
 * @example
 *  <Checkbox>
      <Checkbox.Input/>
      <Checkbox.Label>Remember me</Checkbox.Label>
    </Checkbox>
 */
const Checkbox: FunctionComponent<CheckboxProps> & CheckboxComposition = ({
  children,
  className = '',
  disabled = false,
  errorMsg = '',
}) => {
  return (
    <CheckboxProvider disabled={disabled}>
      <div className="space-y-1">
        <label className={classNames(className, 'flex space-x-3 items-start')}>
          {children}
        </label>
        <span
          className={classNames(
            !errorMsg && 'hidden',
            'text-danger-700 dark:text-danger-500 body4 inline-block'
          )}
          aria-hidden={!errorMsg}
          aria-errormessage={errorMsg}
        >
          {errorMsg}
        </span>
      </div>
    </CheckboxProvider>
  );
};

/**
 * Creates a component to add the input box to the checkbox.
 *
 * @param checked       (false)   Whether the checkbox is checked or not.
 * @param className     ('')      The Styling which should be applied to the component.
 * @param handleChecked (null)    What event should fire, if the checkbox got clicked.
 * @param round         (true)    Whether the checkbox should appear round or square.
 * @param disabled      (false)   Whether the checkbox should be disabled or not.
 *
 * @example <Checkbox.Input/>
 */
const CheckboxInput: FunctionComponent<InputProps> = ({
  defaultChecked = false,
  className = '',
  round = true,
  id = '',
  register = null,
  options = {},
}) => {
  const { disabled } = useCheckbox();
  const [checked, setChecked] = useState(false);

  /**
   * Returns the styling of the checkbox based of the round prop.
   */
  const getVariantStyles = () => {
    if (round) return 'after:rounded-full rounded-full';

    return 'rounded-md after:rounded-md';
  };

  /**
   * Returns the styling of the checkbox based of its checked state.
   */
  const getCheckedStyles = () => {
    if (checked)
      return 'bg-primary-700 dark:bg-primary-500 border-hidden animate-pop after:animate-fade-pop';
  };

  /**
   * Returns the styling of the checkbox based of its disabled state.
   */
  const getDisabledStyles = () => {
    return 'disabled:cursor-default disabled:border-neutral-150 dark:disabled:border-neutral-700';
  };

  /**
   * Returns the hover styling of the checkbox based of its disabled state.
   */
  const getHoverStyles = () => {
    if (disabled) return '';

    return 'before:absolute before:bg-primary-600 dark:before:bg-primary-500 before:rounded-full before:w-5 before:h-5 before:scale-[1.9] before:opacity-0 hover:before:opacity-10';
  };

  return (
    <span
      className={classNames(
        'stack h-5 w-5 pt-[2px] cursor-pointer',
        getHoverStyles()
      )}
    >
      <input
        defaultChecked={defaultChecked}
        disabled={disabled}
        type="checkbox"
        className={classNames(
          'border-neutral-200 after:bg-primary-500 h-5 w-5 appearance-none border-2 stack-item cursor-pointer transition-all stack after:stack-item after:opacity-0 group-disabled:border-accent-600',
          className,
          getDisabledStyles(),
          getCheckedStyles(),
          getVariantStyles()
        )}
        {...register(id ? id : 'checkbox', {
          ...options,
          onChange: () => setChecked(!checked),
        })}
      />

      <Tick checked={checked} className="stack-item h-5 w-5" />
    </span>
  );
};

/**
 * Creates a component to add a label to the checkbox.
 *
 * @param className     ('')      The Styling which should be applied to the component.
 * @param hidden        (false)   Whether the label should be aria only or not.
 *
 * @example  <Checkbox.Label>Remember me</Checkbox.Label>
 */
const CheckboxLabel: FunctionComponent<LabelProps> = ({
  children,
  className = '',
  hidden = false,
}) => {
  const { disabled } = useCheckbox();

  /**
   * Returns the styling of the checkbox based of its disabled state.
   */
  const getDisabledStyles = () => {
    return disabled && 'text-neutral-150 dark:text-neutral-500';
  };

  return (
    <label
      className={classNames(
        className,
        hidden && 'sr-only',
        'select-none text-neutral-800 dark:text-neutral-0 w-[90%] align-top',
        getDisabledStyles()
      )}
    >
      {children}
    </label>
  );
};

Checkbox.Input = CheckboxInput;
Checkbox.Label = CheckboxLabel;

export default Checkbox;
