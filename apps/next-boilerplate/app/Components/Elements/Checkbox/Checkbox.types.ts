/* eslint-disable @typescript-eslint/no-explicit-any */

import { UseFormRegister, RegisterOptions } from 'react-hook-form';

export type TickProps = { checked: boolean; className?: string };

export type CheckboxProps = {
  className?: string;
  disabled?: boolean;
  errorMsg?: string;
};

export type LabelProps = {
  className?: string;
  hidden?: boolean;
};

export type InputProps = {
  defaultChecked?: boolean;
  className?: string;
  round?: boolean;
  id?: string;
  register?: UseFormRegister<any>;
  options?: RegisterOptions;
};
