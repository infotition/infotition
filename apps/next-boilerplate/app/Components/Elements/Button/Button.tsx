import React, { FunctionComponent } from 'react';
import classNames from 'classnames';

import createClickAnimation from '@Utils/createClickAnimation';
import Loader from '@Elements/Loader';
import Icon from '@Elements/Icon';

import type {
  ButtonProps,
  IconProps,
  LabelProps,
  LoaderProps,
} from './Button.types';
import styles from './Button.module.scss';

interface ButtonComposition {
  Label: FunctionComponent<LabelProps>;
  Loader: FunctionComponent<LoaderProps>;
  Icon: FunctionComponent<IconProps>;
}

/**
 * Generates a custom button component.
 *
 * @param type        (button)  The type of the button.
 * @param disabled    (false)   Whether the button should be disabled or not.
 * @param onClick     (null)    The Callback which gets called if the button gets clicked.
 * @param className   ('')      The Styling which should be applied to the component.
 * @param variant     (text)    Defines the style of the button.
 *
 * @example <Button type="submit">...</Button>
 */
const Button: FunctionComponent<ButtonProps> & ButtonComposition = ({
  children,
  disabled = false,
  type = 'button',
  onClick = null,
  className = '',
  variant = 'text',
}) => {
  /**
   * Handles the button click event. It prevents default behaviour and runs a
   * ripple animation and the provided on click handler.
   *
   * @param event The fired click event.
   */
  const handleClick = (event: React.MouseEvent) => {
    // event.preventDefault();
    createClickAnimation(
      event,
      styles.ripple,
      `dark:bg-neutral-0/30 ${
        variant === 'contained' ? 'bg-neutral-0/30' : 'bg-primary-700/30'
      }`
    );
    if (onClick) onClick();
  };

  /**
   * Returns the styling of the button based on the variant prop.
   */
  const getVariantClasses = () => {
    switch (variant) {
      case 'contained':
        return 'bg-primary-700/90 dark:bg-primary-600/80 hover:bg-primary-700 dark:hover:bg-primary-600';
      case 'outlined':
        return 'border-2 border-primary-700/75 hover:border-primary-700 dark:border-primary-200/75 hover:dark:border-primary-200 hover:bg-primary-700/5 dark:hover:bg-primary-200/5';
      case 'text':
        return 'shadow-none hover:bg-primary-700/25 dark:hover:bg-primary-200/25';
    }
  };

  /**
   * Returns the styling of the button based of the disabled state.
   */
  const getDisabledClasses = () => {
    return (
      disabled &&
      'disabled:bg-neutral-100 disabled:border-2 disabled:border-neutral-150 dark:disabled:bg-neutral-700 dark:disabled:border-none'
    );
  };

  return (
    <button
      className={classNames(
        className,
        'shadow-sm w-full rounded-[0.25rem] py-2 flex space-x-3 justify-center items-center transition-all relative overflow-hidden group',
        getVariantClasses(),
        getDisabledClasses()
      )}
      type={type}
      onClick={handleClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

/**
 * Creates a component to add labels to buttons.

 * @param className   ('')      The Styling which should be applied to the component.
 *
 * @example <Button.Label>Sign in</Button.Label>
 */
const Label: FunctionComponent<LabelProps> = ({
  children,
  className = '',
  hidden = false,
}) => {
  return (
    <label
      className={classNames(
        className,
        'text-neutral-0 font-bold body3 group-disabled:text-neutral-600 dark:group-disabled:text-neutral-300 cursor-pointer',
        hidden && 'sr-only'
      )}
    >
      {children}
    </label>
  );
};

/**
 * Creates a component to add a loader animation to the button.

 * @param className   ('')      The Styling which should be applied to the component.
 * @param loading     (false)   If the Button should play a loading animation or not.
 *
 * @example <Button.Loader />
 */
const ButtonLoader: FunctionComponent<LoaderProps> = ({
  className = '',
  loading = false,
}) => {
  return (
    loading && (
      <Loader
        aria-busy={loading}
        className={
          (classNames(className),
          'group-disabled:text-neutral-600 dark:group-disabled:text-neutral-300')
        }
      />
    )
  );
};

/**
 * Creates a component to add a icon to the button.
 *
 * @param className   ('')    The styling which should be applied to the component.
 * @param icon                The rendered icon type.
 * @param size        (30)    The size of the icon.
 *
 * @example <Icon icon="google" />
 */
const ButtonIcon: FunctionComponent<IconProps> = ({
  className = '',
  icon,
  size = 30,
}) => {
  return <Icon icon={icon} dimension={size} className={className} />;
};

Button.Label = Label;
Button.Loader = ButtonLoader;
Button.Icon = ButtonIcon;

export default Button;
