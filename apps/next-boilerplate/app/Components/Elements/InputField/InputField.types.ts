/* eslint-disable @typescript-eslint/no-explicit-any */

import { UseFormRegister, RegisterOptions } from 'react-hook-form';

export type InputFieldProps = {
  className?: string;
  disabled?: boolean;
  errorMsg?: string;
  title?: string;
};

export type InputProps = {
  className?: string;
  type?: 'email' | 'password' | 'text';
  readonly?: boolean;
  value?: string;
  id?: string;
  register?: UseFormRegister<any>;
  options?: RegisterOptions;
};

export type LabelProps = {
  className?: string;
};

export type IconProps = {
  className?: string;
  icon: 'mail' | 'key' | 'password_toggle';
};
