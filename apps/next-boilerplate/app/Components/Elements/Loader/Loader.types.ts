export type LoaderProps = {
  className?: string;
  dimension?: number;
};
