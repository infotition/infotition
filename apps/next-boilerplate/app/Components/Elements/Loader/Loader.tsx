import { FunctionComponent } from 'react';
import classNames from 'classnames';
import { styled, keyframes } from 'goober';

import type { LoaderProps } from './Loader.types';

/**
 * Creates a Loading animation component.

 * @param className   ('')    The Styling which should be applied to the component.
 * @param dimension   (8)     The dimension of the dots.
 *
 * @example <Loader />
 */
const Loader: FunctionComponent<LoaderProps> = ({
  className = '',
  dimension = 8,
}) => {
  const dot1 = keyframes`
  0% { transform: scale(0); }
  100% { transform: scale(1); }
`;

  const dot2 = keyframes`
  0% { transform: translate(0, 0); }
  100% { transform: translate(${dimension + 5}px, 0); }
`;

  const dot3 = keyframes`
  0% { transform: scale(1); }
  100% { transform: scale(0); }
`;

  const dotAnimations = [dot1, dot2, dot3];

  const Dot = styled('div')(
    (props: { pos: number; anim: number }) => `
    position: absolute;
    border-radius: 50px;
    width: ${dimension}px;
    height: ${dimension}px;
    left: ${props.pos * (dimension + 5)}px;
    background-color: currentColor;
    animation: ${dotAnimations[props.anim]} 0.6s infinite;
    transition-timing-function: cubic-bezier(.21,1.14,.83,.67);
  `
  );

  const DotContainer = styled('div')`
    width: ${dimension * 3 + 10}px;
    height: ${dimension}px;
  `;

  return (
    <DotContainer
      className={classNames(
        className,
        'inline-block relative dark:text-neutral-0 text-neutral-800 transition-colors'
      )}
    >
      <Dot pos={0} anim={0} />
      <Dot pos={0} anim={1} />
      <Dot pos={1} anim={1} />
      <Dot pos={2} anim={2} />
    </DotContainer>
  );
};

export default Loader;
