export type DropdownProps = {
  className?: string;
};

export type MenuProps = {
  className?: string;
  active?: boolean;
  isSecondary?: boolean;
};

export type ItemProps = {
  className?: string;
  onClick?: CallableFunction;
  redirect?: string;
};
