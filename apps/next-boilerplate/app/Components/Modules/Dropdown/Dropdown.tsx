/* eslint-disable @typescript-eslint/no-explicit-any */

import classNames from 'classnames';
import { FunctionComponent, useState, useRef } from 'react';
import { CSSTransition } from 'react-transition-group';

import { DropdownProvider, useDropdown } from '@Context/dropdownContext';

import styles from './Dropdown.module.scss';
import type { DropdownProps, MenuProps } from './Dropdown.types';
import { ItemProps } from '@Modules/Navbar/Navbar.types';

interface DropdownComposition {
  Menu: FunctionComponent<MenuProps>;
  Item: FunctionComponent<ItemProps>;
}

/**
 * Dropdown component.
 */
const Dropdown: FunctionComponent<DropdownProps> & DropdownComposition = ({
  children,
  className = '',
}) => {
  const [menuHeight, setMenuHeight] = useState(null);
  const dropdownRef = useRef(null);

  const calcHeight = (el: any) => {
    setMenuHeight(el ? el.offsetHeight : 0);
  };

  return (
    <DropdownProvider calcHeight={calcHeight}>
      <div
        className={classNames(
          className,
          'bg-neutral-800 border-neutral-700 rounded-md absolute -translate-x-[182px] w-[400px] top-14 overflow-hidden transition-[height] duration-500 border-[1px]'
        )}
        style={{ height: menuHeight || 0 }}
        ref={dropdownRef}
      >
        {children}
      </div>
    </DropdownProvider>
  );
};

const Menu: FunctionComponent<MenuProps> = ({
  children,
  className = '',
  active = false,
  isSecondary = false,
}) => {
  const { calcHeight } = useDropdown();

  const transitionRef = useRef(null);

  const getClasses = () => {
    const menu = isSecondary ? 'secondary' : 'primary';

    return {
      enter: styles[`menu-${menu}-enter`],
      enterActive: styles[`menu-${menu}-enter-active`],
      exit: styles[`menu-${menu}-exit`],
      exitActive: styles[`menu-${menu}-exit-active`],
    };
  };

  return (
    <CSSTransition
      in={active}
      unmountOnExit
      timeout={500}
      onEnter={() => calcHeight(transitionRef.current)}
      classNames={getClasses()}
      nodeRef={transitionRef}
    >
      <div ref={transitionRef} className={classNames(className, 'w-full p-3')}>
        {children}
      </div>
    </CSSTransition>
  );
};

const Item: FunctionComponent<ItemProps> = ({
  children,
  className = '',
  onClick = null,
  redirect = '',
}) => {
  return (
    <div className={classNames(className)}>
      <button
        className="bg-neutral-800 w-full flex justify-start hover:brightness-125 p-3 rounded-lg transition-all"
        onClick={() => onClick && onClick()}
      >
        {children}
      </button>
    </div>
  );
};

Dropdown.Menu = Menu;
Dropdown.Item = Item;

export default Dropdown;
