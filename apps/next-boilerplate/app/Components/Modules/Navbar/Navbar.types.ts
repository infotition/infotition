export type NavbarProps = {
  className?: string;
  fixed?: boolean;
};

export type SectionProps = {
  className?: string;
  isList?: boolean;
};

export type ItemProps = {
  className?: string;
};
