import classNames from 'classnames';
import { FunctionComponent } from 'react';

import type { NavbarProps, SectionProps, ItemProps } from './Navbar.types';

interface NavbarComposition {
  Section: FunctionComponent<SectionProps>;
  Item: FunctionComponent<ItemProps>;
}

/**
 * Responsive Navbar component which can be divided into different sections.
 *
 * @param className   ('')      The styling which should be applied to the component.
 * @param fixed       (false)   Whether the navbar should be fixed or not.
 *
 * @example <Navbar><Navbar.Brand redirect="/" /></Navbar>
 */
const Navbar: FunctionComponent<NavbarProps> & NavbarComposition = ({
  children,
  className = '',
  fixed = false,
}) => {
  return (
    <nav
      className={classNames(
        className,
        fixed && 'fixed',
        'dark:bg-neutral-800 shadow-md py-2 px-6 w-full flex justify-between'
      )}
    >
      {children}
    </nav>
  );
};

/**
 * Navbar section to group items so they dont get effected by the
 * space between property of the navbar.
 *
 * @param className   ('')      The styling which should be applied to the component.
 * @param isList      (false)   Whether the section should be wrapped with ul or div.
 *
 * @example <Navbar.Section isList><li>Home</li></Navbar.Section>
 */
const Section: FunctionComponent<SectionProps> = ({
  children,
  className = '',
  isList = false,
}) => {
  const classes = classNames(className, 'flex items-center gap-3');

  if (isList) {
    return <ul className={classes}>{children}</ul>;
  }

  return <div className={classes}>{children}</div>;
};

const Item: FunctionComponent<ItemProps> = ({ children, className = '' }) => {
  return (
    <li className={classNames(className, 'flex items-center justify-center')}>
      {children}
    </li>
  );
};

Navbar.Section = Section;
Navbar.Item = Item;

export default Navbar;
