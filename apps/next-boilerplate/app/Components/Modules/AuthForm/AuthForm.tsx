import { FunctionComponent, useRef } from 'react';
import { useForm, SubmitHandler } from 'react-hook-form';
import Link from 'next/link';
import classNames from 'classnames';
import HCaptcha from '@hcaptcha/react-hcaptcha';

import { InfotitionLogo } from '@Elements/VectorGraphic';
import InputField from '@Elements/InputField';
import Checkbox from '@Elements/Checkbox';
import Button from '@Elements/Button';
import { useAuth } from '@Context/authContext';

import type {
  AuthFormProps,
  SignInProps,
  SignUpProps,
  SocialAuthenticationProps,
} from './AuthForm.types';
import { useI18n } from 'next-localization';

interface AuthFormComposition {
  SignIn: FunctionComponent<SignInProps>;
  SignUp: FunctionComponent<SignUpProps>;
}

const SocialAuthentication: FunctionComponent<SocialAuthenticationProps> = ({
  signInWithProvider = null,
}) => {
  const signInWithGoogle = () =>
    signInWithProvider && signInWithProvider('google');
  const signInWithFacebook = () =>
    signInWithProvider && signInWithProvider('facebook');
  const signInWithGithub = () =>
    signInWithProvider && signInWithProvider('github');

  return (
    <div className="flex space-x-5">
      <Button type="button" variant="outlined" onClick={signInWithGoogle}>
        <Button.Icon icon="google" />
        <Button.Label hidden>Authenticate with Google</Button.Label>
      </Button>

      <Button type="button" variant="outlined" onClick={signInWithFacebook}>
        <Button.Icon icon="github" />
        <Button.Label hidden>Authenticate with Github</Button.Label>
      </Button>

      <Button type="button" variant="outlined" onClick={signInWithGithub}>
        <Button.Icon icon="facebook" />
        <Button.Label hidden>Authenticate with Facebook</Button.Label>
      </Button>
    </div>
  );
};

/**
 * Auth module wrapper to choose between sign in and sign up.
 *
 * @param className ('') The styling which should be applied to the component.
 *
 * @example <Auth><Auth.SignUp /></Auth>
 */
const AuthForm: FunctionComponent<AuthFormProps> & AuthFormComposition = ({
  children,
  className = '',
}) => {
  return (
    <div className={classNames(className, 'space-y-10')}>
      <InfotitionLogo className="text-primary-700 dark:text-primary-200 hidden desktop:block" />
      {children}
    </div>
  );
};

/**
 * Adds a sign in form to the auth form wrapper which handles email and
 * social sing in options.
 *
 * @param className ('') The styling which should be applied to the component.
 *
 * @example <Auth.SignIn />
 */
const SignIn: FunctionComponent<SignInProps> = ({ className = '' }) => {
  type Inputs = {
    email: string;
    password: string;
    remember: boolean;
  };

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<Inputs>();

  const i18n = useI18n();

  const { details, functions } = useAuth();

  const captchaRef = useRef(null);

  /**
   * Executes the captcha which must be completed to submit
   * the form.
   */
  const onSubmit: SubmitHandler<Inputs> = async () => {
    captchaRef.current.execute();
  };

  /**
   * Verifies the captcha token and signs in the user with mail
   * if the token got verified.
   */
  const onCaptchaChange = async (captchaCode: string) => {
    functions.signIn(getValues(), captchaCode);
  };

  return (
    <div className={classNames(className, 'space-y-10')}>
      <h2 className="header3m font-bold tablet:header4">
        {i18n.t('sign_in.welcome')}
      </h2>

      <section className="space-y-2" aria-label="Social Sign up">
        <span className="body3 font-medium">{i18n.t('sign_in.sign_with')}</span>
        <SocialAuthentication
          signInWithProvider={functions.signInWithProvider}
        />
      </section>

      <div className="text-neutral-600 dark:text-neutral-500 grid grid-cols-4 items-center tablet:grid-cols-3">
        <span aria-hidden className="h-[1px] bg-neutral-600" />
        <h3 className="text-center col-span-2 tablet:col-span-1">
          {i18n.t('sign_in.continue_with')}
        </h3>
        <span aria-hidden className="h-[1px] bg-neutral-600" />
      </div>

      <form noValidate className="space-y-5" onSubmit={handleSubmit(onSubmit)}>
        <InputField
          title={i18n.t('sign_in.email')}
          errorMsg={errors.email?.message}
        >
          <InputField.Icon icon="mail" />
          <InputField.Input type="email" register={register} />
          <InputField.Label />
        </InputField>

        <InputField
          title={i18n.t('sign_in.password')}
          errorMsg={errors.password?.message}
        >
          <InputField.Icon icon="key" />
          <InputField.Input type="password" register={register} />
          <InputField.Label />
          <InputField.Icon icon="password_toggle" />
        </InputField>

        <div className="flex justify-between">
          <Checkbox>
            <Checkbox.Input register={register} id="remember" round={false} />
            <Checkbox.Label>{i18n.t('sign_in.remember')}</Checkbox.Label>
          </Checkbox>

          <Link href={'/password-reset'}>
            <a className="after-underline after:bg-primary-500 text-primary-700 dark:text-primary-200">
              {i18n.t('sign_in.forgot')}
            </a>
          </Link>
        </div>

        <HCaptcha
          id="sign-up-captcha"
          sitekey={process.env.NEXT_PUBLIC_HCAPTCHA_SITE_KEY}
          ref={captchaRef}
          size="invisible"
          onVerify={onCaptchaChange}
        />

        <Button type="submit" variant="contained">
          <Button.Loader loading={details.loading} />
          <Button.Label> {i18n.t('sign_in.sign_in')}</Button.Label>
        </Button>
      </form>
    </div>
  );
};

/**
 * Adds a sign up form to the auth form wrapper which handles email and
 * social sing up options.
 *
 * @param className ('') The styling which should be applied to the component.
 *
 * @example <Auth.SignUp />
 */
const SignUp: FunctionComponent<SignUpProps> = ({ className = '' }) => {
  type Inputs = {
    email: string;
    password: string;
    agreement: boolean;
  };

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<Inputs>();

  const { details, functions } = useAuth();
  const i18n = useI18n();

  const captchaRef = useRef(null);

  /**
   * Executes the captcha which must be completed to submit
   * the form.
   */
  const onSubmit: SubmitHandler<Inputs> = async () => {
    captchaRef.current.execute();
  };

  /**
   * Verifies the captcha token and signs up the user with mail
   * if the token got verified.
   */
  const onCaptchaChange = async (captchaCode: string) => {
    functions.signUp(getValues(), captchaCode);
  };

  const emailValidation = {
    required: {
      message: 'Your email is required.',
      value: true,
    },
    pattern: {
      message: 'Please provide a valid email.',
      value:
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    },
    maxLength: {
      message: 'Your email can not be longer than 50 characters.',
      value: 50,
    },
  };

  const passwordValidation = {
    required: {
      message: 'Your password is required.',
      value: true,
    },
    minLength: {
      message: 'Your password must be at least 5 characters long.',
      value: 5,
    },
    maxLength: {
      message: 'Your password can not be longer than 50 characters.',
      value: 50,
    },
  };

  const agreementValidation = {
    validate: (val: boolean) => val || 'Please accept or requirements.',
  };

  return (
    <div className={classNames(className, 'space-y-10')}>
      <h2 className="header3m font-bold tablet:header4">
        {i18n.t('sign_up.welcome')}
      </h2>

      <section className="space-y-2" aria-label="Social Sign up">
        <span className="body3 font-medium">{i18n.t('sign_up.sign_with')}</span>
        <SocialAuthentication
          signInWithProvider={functions.signInWithProvider}
        />
      </section>

      <div className="text-neutral-600 dark:text-neutral-500 grid grid-cols-4 items-center tablet:grid-cols-3">
        <span aria-hidden className="h-[1px] bg-neutral-600" />
        <h3 className="text-center col-span-2 tablet:col-span-1">
          {i18n.t('sign_up.continue_with')}
        </h3>
        <span aria-hidden className="h-[1px] bg-neutral-600" />
      </div>

      <form noValidate className="space-y-5" onSubmit={handleSubmit(onSubmit)}>
        <InputField
          title={i18n.t('sign_up.email')}
          errorMsg={errors.email?.message}
        >
          <InputField.Icon icon="mail" />
          <InputField.Input
            type="email"
            register={register}
            options={emailValidation}
          />
          <InputField.Label />
        </InputField>

        <InputField
          title={i18n.t('sign_up.password')}
          errorMsg={errors.password?.message}
        >
          <InputField.Icon icon="key" />
          <InputField.Input
            type="password"
            register={register}
            options={passwordValidation}
          />
          <InputField.Label />
          <InputField.Icon icon="password_toggle" />
        </InputField>

        <Checkbox errorMsg={errors.agreement?.message}>
          <Checkbox.Input
            register={register}
            id="agreement"
            options={agreementValidation}
            round={false}
          />
          <Checkbox.Label>
            {i18n.t('sign_up.agreement')}
            <Link href="/tos">
              <a className="after-underline after:bg-primary-500 text-primary-700 dark:text-primary-200">
                {i18n.t('sign_up.tos')}
              </a>
            </Link>
            {i18n.t('sign_up.and_the')}
            <Link href="/privacy">
              <a className="after-underline after:bg-primary-500 text-primary-700 dark:text-primary-200">
                {i18n.t('sign_up.privacy')}
              </a>
            </Link>
            .
          </Checkbox.Label>
        </Checkbox>

        <HCaptcha
          id="sign-up-captcha"
          sitekey={process.env.NEXT_PUBLIC_HCAPTCHA_SITE_KEY}
          ref={captchaRef}
          size="invisible"
          onVerify={onCaptchaChange}
        />

        <Button type="submit" variant="contained">
          <Button.Loader loading={details.loading} />
          <Button.Label>{i18n.t('sign_up.sign_up')}</Button.Label>
        </Button>
      </form>
    </div>
  );
};

AuthForm.SignIn = SignIn;
AuthForm.SignUp = SignUp;

export default AuthForm;
