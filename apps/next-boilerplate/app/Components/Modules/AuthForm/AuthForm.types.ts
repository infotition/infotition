import { Provider } from '@supabase/supabase-js';

export type AuthFormProps = {
  className?: string;
};

export type SignInProps = {
  className?: string;
};

export type SignUpProps = {
  className?: string;
};

export type SocialAuthenticationProps = {
  signInWithProvider?: (provider: Provider) => Promise<void>;
};
