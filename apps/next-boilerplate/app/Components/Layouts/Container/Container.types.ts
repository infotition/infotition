export type ContainerProps = {
  className?: string;
};

export type CenterProps = {
  className?: string;
  fullPage?: boolean;
  isVCentered?: boolean;
  isHCentered?: boolean;
};
