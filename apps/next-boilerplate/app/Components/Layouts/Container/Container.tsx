import { FunctionComponent } from 'react';
import classNames from 'classnames';

import type { ContainerProps, CenterProps } from './Container.types';

interface ContainerComposition {
  Center: FunctionComponent<CenterProps>;
}

/**
 * Container component with min screen height.
 *
 * @param className   ('')    The styling which should be applied to the component.
 *
 * @example <Container>Hello</Container>
 */
const Container: FunctionComponent<ContainerProps> & ContainerComposition = ({
  children,
  className = '',
}) => {
  return (
    <div
      className={classNames(
        className,
        'bg-neutral-0 dark:bg-neutral-800 text-neutral-800 dark:text-neutral-0 min-h-screen transition-colors duration-150'
      )}
    >
      {children}
    </div>
  );
};

/**
 * Adds a component to the container to center the children with wrapped
 * tailwind container.
 *
 * @param className     ('')      The styling which should be applied to the component.
 * @param fullPage      (false)   If the container should be full width or responsive.
 * @param isVCentered   (true)    If the content should be centered vertically or not.
 * @param isHCentered   (true)    If the content should be centered horizontally or not.
 *
 * @example <Container.Center>Hello</Container.Center>
 */
const Center: FunctionComponent<CenterProps> = ({
  children,
  className = '',
  fullPage = false,
  isVCentered = true,
  isHCentered = true,
}) => {
  return (
    <div
      className={classNames('min-h-screen flexable', className, {
        'mx-auto': isHCentered,
        'justify-center items-center space-y-5': isVCentered,
        'container py-8': !fullPage,
      })}
    >
      {children}
    </div>
  );
};

Container.Center = Center;

export default Container;
