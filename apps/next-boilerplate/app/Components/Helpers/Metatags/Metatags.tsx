import { FunctionComponent } from 'react';

import type { MetatagsProps } from './Metatags.types';

/**
 * Sets various seo meta information for the page including twitter, facebook, ... card
 * information and responsive tags.
 *
 * @param appName     ('')  The name of the web application.
 * @param desc        ('')  The description of the web application.
 * @param domain      ('')  The domain of the web application.
 * @param themeColor  ('')  The primary theme color of the web application.
 *
 * @example <Metatags appName="Infotition" />
 */
const Metatags: FunctionComponent<MetatagsProps> = ({
  appName = '',
  desc = '',
  domain = '',
  themeColor = '',
}) => (
  <>
    {/* PAGE META TAGS */}
    <meta name="application-name" content={appName} />
    <meta
      name="viewport"
      content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, viewport-fit=cover"
    />
    <meta name="description" content={desc} />
    <meta name="theme-color" content={themeColor} />
    <link rel="manifest" href="/manifest.json" />
    <link rel="shortcut icon" href="/favicon.ico" />

    {/* MOBILE META TAGS */}
    <meta name="format-detection" content="telephone=no" />
    <meta name="mobile-web-app-capable" content="yes" />

    {/* APPLE META TAGS */}
    <meta name="apple-mobile-web-app-title" content={appName} />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="default" />
    <link rel="apple-touch-icon" href="/favicons/touch-icon-iphone.png" />
    <link
      rel="apple-touch-icon"
      sizes="152x152"
      href="/favicons/touch-icon-ipad.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="180x180"
      href="/favicons/touch-icon-iphone-retina.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="180x180"
      href="/favicons/touch-icon-iphone.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="167x167"
      href="/favicons/touch-icon-ipad-retina.png"
    />
    <link
      rel="mask-icon"
      href="/favicons/safari-pinned-tab.svg"
      color={themeColor}
    />

    {/* WINDOWS META TAGS */}
    <meta name="msapplication-TileColor" content={themeColor} />
    <meta name="msapplication-tap-highlight" content="no" />

    {/* TWITTER META TAGS */}
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:url" content={domain} />
    <meta name="twitter:title" content={appName} />
    <meta name="twitter:description" content={desc} />
    <meta
      name="twitter:image"
      content={`${domain}/favicons/android-chrome-192x192.png`}
    />
    <meta name="twitter:creator" content="@DavidWShadow" />

    {/* OPEN GRAPH META TAGS */}
    <meta property="og:type" content="website" />
    <meta property="og:title" content={appName} />
    <meta property="og:description" content={desc} />
    <meta property="og:site_name" content={appName} />
    <meta property="og:url" content={domain} />
    <meta
      property="og:image"
      content={`${domain}/favicons/android-chrome-192x192.png`}
    />
  </>
);

export default Metatags;
