export type MetatagsProps = {
  appName?: string;
  desc?: string;
  domain?: string;
  themeColor?: string;
};
