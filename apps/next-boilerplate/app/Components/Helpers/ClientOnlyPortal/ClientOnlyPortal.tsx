import { useRef, useEffect, FunctionComponent } from 'react';
import { createPortal } from 'react-dom';

import useIsMounted from '@Hooks/useIsMounted';

import type { ClientOnlyPortalProps } from './ClientOnlyPortal.types';

/**
 * Mounts the children to the portal with the provided selector.
 *
 * @param selector  The selector for the portal component.
 *
 * @example <ClientOnlyPortal selector="modal">Hello World</ClientOnlyPortal>
 */
const ClientOnlyPortal: FunctionComponent<ClientOnlyPortalProps> = ({
  children,
  selector,
}) => {
  const ref = useRef();
  const mounted = useIsMounted();

  useEffect(() => {
    ref.current = document.querySelector(selector);
  }, [selector]);

  return mounted ? createPortal(children, ref.current) : null;
};

export default ClientOnlyPortal;
