import { FunctionComponent } from 'react';
import Link from 'next/link';

import Container from '@Layouts/Container';
import ThemeSwitch from '@Elements/ThemeSwitch';

const HomeTemplate: FunctionComponent = () => {
  return (
    <Container>
      <ThemeSwitch position="upper_right" />
      <Container.Center>
        <h1 className="header1m font-bold text-center tablet:header1">
          Infotition Next.js Boilerplate 💡
        </h1>

        <h2 className="text-neutral-700 dark:text-neutral-0 header4m space-x-3 text-center tablet:header4">
          <span>Made with ❤️ by</span>
          <Link href="https://gitlab.com/infotition">
            <a
              className="after-underline after:bg-primary-500"
              target="_blank"
              rel="noreferrer"
            >
              Infotition
            </a>
          </Link>
        </h2>
      </Container.Center>
    </Container>
  );
};

export default HomeTemplate;
