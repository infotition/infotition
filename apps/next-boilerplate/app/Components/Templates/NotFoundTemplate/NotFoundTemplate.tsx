import ArrowRightIcon from '@heroicons/react/solid/esm/ArrowRightIcon';
import Link from 'next/link';
import { useI18n } from 'next-localization';
import { FunctionComponent } from 'react';
import Image from 'next/image';

import ThemeSwitch from '@Elements/ThemeSwitch';
import Container from '@Layouts/Container';
import { InfotitionLogo } from '@Elements/VectorGraphic';
import BottomNav from '@Modules/BottomNav';

const NotFoundTemplate: FunctionComponent = () => {
  const i18n = useI18n();

  return (
    <Container>
      <ThemeSwitch position="upper_right" />
      <Container.Center className="tablet:relative justify-between">
        <InfotitionLogo className="text-primary-700 dark:text-primary-200 hidden absolute tablet:block tablet:absolute tablet:left-4" />

        <main className="flex items-center h-[95%]">
          <div className="w-full space-y-12 text-center flex flex-col items-center tablet:items-start tablet:text-left tablet:max-w-xl">
            <span className="text-primary-700 dark:text-primary-200 body2 font-semibold uppercase desktop:header5">
              {i18n.t('404.404_error')}
            </span>
            <section className="space-y-4">
              <h1 className="text-neutral-800 dark:text-neutral-0 font-bold header1m desktop:header1">
                {i18n.t('404.not_found')}
              </h1>
              <p className="text-neutral-700 dark:text-neutral-150 body3 desktop:body2">
                {i18n.t('404.not_found_sorry')}
              </p>
            </section>
            <Link href="/">
              <a className="text-primary-700 dark:text-primary-200 body3 desktop:body2 font-medium flex space-x-3 w-fit items-center group">
                <span>{i18n.t('404.back_home')}</span>
                <ArrowRightIcon className="h-5 w-5 group-hover:animate-bounce-right" />
              </a>
            </Link>
          </div>
          <span
            aria-hidden
            className="hidden absolute tablet:block tablet:static"
          >
            <Image
              src="/images/error_dog.svg"
              alt={i18n.t('404.error_dog')}
              width={1000}
              height={700}
            />
          </span>
        </main>

        <footer>
          <BottomNav>
            <BottomNav.Link href="/contact" withDivider>
              Support
            </BottomNav.Link>
            <BottomNav.Link href="api/health-check" withDivider>
              Status
            </BottomNav.Link>
            <BottomNav.Link href="https://twitter.com/infotition">
              Twitter
            </BottomNav.Link>
          </BottomNav>
        </footer>
      </Container.Center>
    </Container>
  );
};

export default NotFoundTemplate;
