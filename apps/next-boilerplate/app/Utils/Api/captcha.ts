import phin from 'phin';

/**
 * Verifies the provided captcha token and returns, if the validation
 * was successful or not.
 *
 * @param captchaToken The hCaptcha token which should get verified.
 *
 * @return True if the token was valid, false instead.
 */
export const validateCaptcha = async (captchaToken: string) => {
  type HCaptchaResponse = {
    success: boolean;
  };

  const { body: captchaValidation } = await phin<HCaptchaResponse>({
    url: 'https://hcaptcha.com/siteverify',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
    },
    data: `response=${captchaToken}&secret=${process.env.HCAPTCHA_SECRET_KEY}`,
    parse: 'json',
  });

  return captchaValidation.success;
};
