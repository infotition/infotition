import gravatar from 'gravatar';

/**
 * Generates a url to the gravatar related to the provided email.
 *
 * @param email The email of the related gravatar.
 * @returns The url to the avatar of the user with the given email.
 */
export const generateGravatarUrl = (email: string) => {
  return 'https:' + gravatar.url(email, { r: 'g', d: 'retro' });
};

/**
 * Fetches the image from the url and provides it as array buffer.
 *
 * @param url The url to fetch the image from.
 * @returns An array buffer representing the fetched image.
 */
export const getImageFromUrl = async (url: string) => {
  return (await fetch(url)).arrayBuffer();
};
