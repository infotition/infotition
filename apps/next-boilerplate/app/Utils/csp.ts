/* eslint-disable @typescript-eslint/no-explicit-any */

import crypto from 'crypto';

const isProd = process.env.NODE_ENV === 'production';

/**
 * Provides csp headers for the document.
 */
export const getCsp = (inlineScriptSource: any) => {
  const csp = [];
  const hash = crypto.createHash('sha256').update(inlineScriptSource);

  csp.push(`base-uri 'self'`);
  csp.push(`form-action 'self'`);
  csp.push(`default-src 'self'`);
  csp.push(
    `script-src 'self' ${isProd ? '' : ` 'unsafe-eval'`} 'sha256-${hash.digest(
      'base64'
    )}' https://hcaptcha.com https://*.hcaptcha.com data:`
  );
  csp.push(
    `style-src 'self' ${
      isProd ? '' : ` 'unsafe-inline'`
    } https://hcaptcha.com https://*.hcaptcha.com`
  );
  csp.push(
    `connect-src 'self' vitals.vercel-insights.com https://hcaptcha.com https://*.hcaptcha.com hivdoxiiruewmnqmbxca.supabase.co`
  );
  csp.push(`img-src 'self' data:`);
  csp.push(`font-src 'self' data:`);
  csp.push(`frame-src * https://hcaptcha.com https://*.hcaptcha.com`);
  csp.push(`media-src * https://hcaptcha.com https://*.hcaptcha.com`);

  return csp.join('; ');
};
