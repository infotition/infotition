const withTM = require('next-transpile-modules')(['utils']);
const withPWA = require('next-pwa');
const runtimeCaching = require('next-pwa/cache');
const withPlugins = require('next-compose-plugins');
const { createSecureHeaders } = require('next-secure-headers');

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

const isDev = process.env.NODE_ENV !== 'production';

const getFontCache = () => {
  const fonts = [
    '/fonts/Poppins-RegularItalic.woff2',
    '/fonts/Poppins-MediumItalic.woff2',
    '/fonts/Poppins-SemiboldItalic.woff2',
    '/fonts/Poppins-BoldItalic.woff2',
    '/fonts/Poppins-Regular.woff2',
    '/fonts/Poppins-Medium.woff2',
    '/fonts/Poppins-Semibold.woff2',
    '/fonts/Poppins-Bold.woff2',
    '/fonts/Inter-Regular.woff2',
    '/fonts/Inter-Medium.woff2',
    '/fonts/Inter-Semibold.woff2',
    '/fonts/Inter-Bold.woff2',
  ];

  return fonts.map((font) => {
    return {
      source: font,
      headers: [
        {
          key: 'Cache-control',
          value: 'public, immutable, max-age=31536000',
        },
      ],
    };
  });
};

const nextConfig = {
  reactStrictMode: true,
  pwa: {
    dest: 'public',
    runtimeCaching,
    buildExcludes: [/manifest.json$/],
    register: true,
    disable: isDev,
  },
  i18n: {
    locales: ['en', 'de'],
    defaultLocale: 'en',
  },
  images: {
    domains: ['hivdoxiiruewmnqmbxca.supabase.in'],
  },
  async headers() {
    return [
      {
        source: '/:path*',
        headers: createSecureHeaders(),
      },
      ...getFontCache(),
    ];
  },
};

module.exports = withPlugins(
  [withPWA, [withBundleAnalyzer], withTM],
  nextConfig
);
