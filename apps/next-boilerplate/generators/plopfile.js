module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Create a component.',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?',
      },
      {
        type: 'list',
        name: 'type',
        message: 'What is your component type?',
        choices: ['Element', 'Helper', 'Layout', 'Module', 'Template'],
        filter: (val) => val + 's',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../app/Components/{{type}}/{{pascalCase name}}/index.ts',
        templateFile: 'templates/component/index.ts.hbs',
      },
      {
        type: 'add',
        path: '../app/Components/{{type}}/{{pascalCase name}}/{{pascalCase name}}.tsx',
        templateFile: 'templates/component/component.tsx.hbs',
      },
      {
        type: 'add',
        path: '../app/Components/{{type}}/{{pascalCase name}}/{{pascalCase name}}.types.ts',
        templateFile: 'templates/component/component.types.ts.hbs',
      },
    ],
  });

  plop.setGenerator('page', {
    description: 'Create a next page.',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your name of the page?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../pages/{{kebabCase name}}.tsx',
        templateFile: 'templates/page/page.tsx.hbs',
      },
    ],
  });

  plop.setGenerator('context', {
    description: 'Create a react context.',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your context name?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../app/Context/{{camelCase name}}Context.tsx',
        templateFile: 'templates/context/context.tsx.hbs',
      },
    ],
  });
};
