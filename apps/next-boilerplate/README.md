# Infotition Next.js Boilerplate

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https%3A%2F%2Fgitlab.com%2Finfotition%2Finfotition%2F-%2Ftree%2Fmain%2Fapps%2Fnext-boilerplate&demo-title=Infotition%20Next.js%20Boilerplate&demo-description=Ready%20for%20production%20Next.js%20Boilerplate%20with%20typescript%2C%20scss%2C%20tailwind%2C%20linting%2C%20jest%20and%20pwa%20configuration.)
