module.exports = {
  siteUrl: process.env.SITE_URL || 'https://infotition.de',
  generateRobotsTxt: true,
};
