/* eslint-disable @typescript-eslint/no-explicit-any */

import Document, { Html, Main, NextScript, Head } from 'next/document';
import { extractCss } from 'goober';
import { FunctionComponent } from 'react';

import Metatags from '@Helpers/Metatags';
import { getCsp } from '@Utils/csp';

const Fonts: FunctionComponent = () => {
  const fonts = [
    '/fonts/Poppins-RegularItalic.woff2',
    '/fonts/Poppins-MediumItalic.woff2',
    '/fonts/Poppins-SemiboldItalic.woff2',
    '/fonts/Poppins-BoldItalic.woff2',
    '/fonts/Poppins-Regular.woff2',
    '/fonts/Poppins-Medium.woff2',
    '/fonts/Poppins-Semibold.woff2',
    '/fonts/Poppins-Bold.woff2',
    '/fonts/Inter-Regular.woff2',
    '/fonts/Inter-Medium.woff2',
    '/fonts/Inter-Semibold.woff2',
    '/fonts/Inter-Bold.woff2',
  ];

  return (
    <>
      {fonts.map((font) => {
        return (
          <link
            key={font}
            rel="preload"
            href={font}
            as="font"
            type="font/woff2"
            crossOrigin="anonymous"
          />
        );
      })}
    </>
  );
};

class MyDocument extends Document {
  static async getInitialProps({ renderPage }) {
    const page = await renderPage();
    const css = extractCss();
    return { ...page, css };
  }

  render() {
    return (
      <Html>
        <Head>
          <meta name="referrer" content={'strict-origin'} />
          <meta
            httpEquiv="Content-Security-Policy"
            content={getCsp(NextScript.getInlineScriptSource(this.props))}
          />
          <Metatags
            appName="Infotition"
            desc="Ready for production Next.js Boilerplate with typescript, scss, tailwind, linting, jest and pwa configuration."
            domain="https://infotition.de"
            themeColor="#5927E5"
          />
          <Fonts />
          <style
            id={'_goober'}
            dangerouslySetInnerHTML={{
              __html: ' ' + (this.props as any).css,
            }}
          />
        </Head>

        <body>
          <Main />
          <div id="theme-switch" />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
