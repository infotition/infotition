import { useI18n } from 'next-localization';

import Title from '@Helpers/Title';
import NotFoundTemplate from '@Templates/NotFoundTemplate';

const Custom404Page = () => {
  const i18n = useI18n();

  return (
    <>
      <Title title={i18n.t('404.not_found')} />
      <NotFoundTemplate />
    </>
  );
};

export async function getStaticProps({ locale }) {
  const { default: lngDict = {} } = await import(
    `../public/locales/${locale}.json`
  );

  return { props: { lngDict } };
}

export default Custom404Page;
