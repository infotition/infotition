import { AppProps, NextWebVitalsMetric } from 'next/app';
import { useRouter } from 'next/router';
import { ThemeProvider } from 'next-themes';
import React, { useEffect } from 'react';
import { innerVh } from 'inner-vh';
import { deviceType } from 'detect-it';
import { I18nProvider } from 'next-localization';
import { prefix } from 'goober-autoprefixer';
import { setup } from 'goober';
import { Toaster } from 'react-hot-toast';

import { AuthProvider } from '@Context/authContext';

import '@Styles/base.scss';

import 'tippy.js/dist/tippy.css';
import 'tippy.js/animations/shift-away.css';

setup(React.createElement, prefix);

const App = ({ Component, pageProps }: AppProps) => {
  const router = useRouter();

  // Override the vh unit so it is also full height on mobile.
  useEffect(() => {
    innerVh({
      customPropertyName: 'inner-vh',
      ignoreCollapsibleUi: deviceType === 'touchOnly',
      maximumCollapsibleUiHeight: 120,
    });
  }, []);

  const { lngDict } = pageProps;

  return (
    <I18nProvider lngDict={lngDict} locale={router.locale}>
      <ThemeProvider attribute="class" enableSystem={false}>
        <AuthProvider>
          <Component {...pageProps} />
          <Toaster
            toastOptions={{
              className:
                'dark:bg-neutral-900 text-neutral-800 dark:text-neutral-0',
            }}
          />
        </AuthProvider>
      </ThemeProvider>
    </I18nProvider>
  );
};

export function reportWebVitals(metric: NextWebVitalsMetric) {
  console.log(metric);
}

export default App;
