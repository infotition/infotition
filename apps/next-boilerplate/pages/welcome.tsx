import { useAuth } from '@Context/authContext';
import Title from '@Helpers/Title';
import { useEffect } from 'react';

const Welcome = () => {
  const { details } = useAuth();

  useEffect(() => {
    if (details.authCookieSet) {
      fetch('/api/verify', {
        method: 'POST',
        headers: new Headers({ 'Content-Type': 'application/json' }),
        credentials: 'same-origin',
      });
    }
  }, [details.authCookieSet]);

  return (
    <>
      <Title title={'Welcome'} />
      <h1>Welcome</h1>
    </>
  );
};

export default Welcome;
