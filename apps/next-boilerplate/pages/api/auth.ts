import { createHandler, Post, Req, Res } from '@storyofams/next-api-decorators';
import { NextApiRequest, NextApiResponse } from 'next';

import { supabase } from '@Lib/initSubabase';

class Auth {
  /**
   * Sets the auth cookie based on the AuthChangeEvent in the client.
   * This needs to get called, so the supabase api can fetch the user
   * from cookie data.
   *
   * @memberof Auth
   */
  @Post() auth(@Req() req: NextApiRequest, @Res() res: NextApiResponse) {
    supabase.auth.api.setAuthCookie(req, res);
  }
}

export default createHandler(Auth);
