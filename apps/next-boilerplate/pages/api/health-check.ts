import { createHandler, Get } from '@storyofams/next-api-decorators';

class HealthCheck {
  /**
   * Returns a positive status, if the server is running.
   *
   * @return The status of the server. No response means, that the server
   * is not running.
   *
   * @memberof HealthCheck
   */
  @Get() healthCheck() {
    return { status: 'OK' };
  }
}

export default createHandler(HealthCheck);
