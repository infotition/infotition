import { NextApiRequest, NextApiResponse } from 'next';
import {
  UnauthorizedException,
  createHandler,
  Post,
  Req,
  Res,
} from '@storyofams/next-api-decorators';

import { generateGravatarUrl, getImageFromUrl } from '@Utils/Api/gravatar';
import { adminbase } from '@Lib/initAdminSupabase';

class Verify {
  /**
   * Reads the user data from cookie. If the user doesn't have an avatar, it fetches
   * the gravatar from user mail and saves it into a public avatar bucket. The avatar_url
   * field of the user gets updated to his fetched avatar.
   *
   * @memberof Verify
   */
  @Post() async verify(
    @Req() req: NextApiRequest,
    @Res() res: NextApiResponse
  ) {
    const { user } = await adminbase.auth.api.getUserByCookie(req, res);

    if (!user) throw new UnauthorizedException();

    // Get avatar url field, so it only uploads a new image if the field is null
    // which means, that no image was uploaded to this point
    const { body } = await adminbase
      .from('users')
      .select('avatar_url')
      .eq('id', user.id)
      .limit(1)
      .single();

    if (!body.avatar_url) {
      // Generate and download avatar from gravatar with corresponding mail
      const img = await getImageFromUrl(generateGravatarUrl(user.email));

      // Save the avatar in public container with id as name and get
      // its public url.
      await adminbase.storage.from('avatars').upload(`${user.id}.png`, img, {
        contentType: 'image/png',
      });

      const { publicURL } = adminbase.storage
        .from('avatars')
        .getPublicUrl(`${user.id}.png`);

      // Update the avatar url in the users table to point to the avatar
      // generated in the bucket.
      await adminbase
        .from('users')
        .update({ avatar_url: publicURL })
        .match({ id: user.id });
    }
  }
}

export default createHandler(Verify);
