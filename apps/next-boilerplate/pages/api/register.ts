import { Body, createHandler, Post } from '@storyofams/next-api-decorators';

import { supabase } from '@Lib/initSubabase';
import { validateCaptcha } from '@Utils/Api/captcha';

type RegisterInput = {
  email: string;
  password: string;
  captchaToken: string;
};

class Register {
  /**
   * Validates the hCaptcha token. If the token is valid, it sends
   * the sign up payload to supabase to register a new account.
   *
   * @return The supabase signUp payload or a 442 error,
   * if the captcha token was invalid.
   *
   * @memberof Register
   */
  @Post() async register(@Body() body: RegisterInput) {
    const { email, password, captchaToken } = body;

    if (await validateCaptcha(captchaToken)) {
      return supabase.auth.signUp({ email, password });
    }

    return {
      error: { status: 442, message: 'Captcha token is invalid.' },
    };
  }
}

export default createHandler(Register);
