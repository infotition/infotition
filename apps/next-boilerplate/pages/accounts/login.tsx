import Image from 'next/image';
import HomeIcon from '@heroicons/react/solid/esm/HomeIcon';

import Auth from '@Modules/AuthForm';
import Container from '@Layouts/Container';
import { ThemeSwitchPosition } from '@Elements/ThemeSwitch';
import Button from '@Elements/Button';
import {
  AbstractDotPattern,
  AbstractCirclePattern,
} from '@Elements/VectorGraphic';

import styles from './Login.module.scss';
import { useEffect, useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

const Login = () => {
  const router = useRouter();
  const [isSignUp, setIsSignUp] = useState(false);

  useEffect(() => {
    const { signIn } = router.query;
    setIsSignUp(signIn !== 'true');
  }, [router]);

  return (
    <Container
      hasThemeSwitch
      fullPage
      themeSwitchPosition={ThemeSwitchPosition.UPPER_LEFT}
      className="flexable items-center"
    >
      <main
        className={`h-full w-full ${styles.container} ${
          isSignUp && styles['right-panel-active']
        }`}
      >
        {/* SIGN UP CONTAINER */}
        <div
          className={`flexable justify-center items-center ${styles['form-container']} ${styles['sign-up-container']}`}
        >
          <div className="relative flexable justify-center items-center w-full h-full">
            <div className="flex w-full justify-end items-center space-x-10 px-7 absolute top-5">
              <span className="text-neutral-600 dark:text-neutral-400 body4 font-medium align-middle">
                Already have an account?
              </span>
              <Button
                className="w-32"
                aria-label="sign up"
                type="button"
                variant="contained"
                onClick={() => {
                  setIsSignUp(false);
                }}
              >
                Sign In
              </Button>
            </div>
            <Auth className="w-4/5 tablet:w-2/3 desktop:max-w-3xl" isSignUp />
          </div>
        </div>

        {/* SIGN IN CONTAINER */}
        <div
          className={`flexable justify-center items-center ${styles['form-container']} ${styles['sign-in-container']}`}
        >
          <div className="relative flexable justify-center items-center w-full h-full">
            <div className="flex w-full justify-end items-center space-x-10 px-7 absolute top-5">
              <span className="text-neutral-600 dark:text-neutral-400 body4 font-medium align-middle">
                Not registered yet?
              </span>
              <Button
                className="w-32"
                aria-label="sign up"
                type="button"
                variant="contained"
                onClick={() => {
                  setIsSignUp(true);
                }}
              >
                Sign Up
              </Button>
            </div>
            <Auth className="w-4/5 tablet:w-2/3 desktop:max-w-3xl" />
          </div>
        </div>

        <div className={`${styles['overlay-container']}`}>
          <div
            className={`bg-gradient-to-r from-primary-200 to-primary-100 dark:from-primary-600 dark:to-primary-500 ${styles.overlay}`}
          >
            <div
              className={`flexable justify-center ${styles['overlay-panel']} ${styles['overlay-left']}`}
            >
              <div className="relative flexable pt-16 items-center h-full w-full">
                <div className="absolute top-6 left-[48.4%]">
                  <Link href="/">
                    <a className=" text-primary-700 dark:text-primary-200">
                      <HomeIcon className="text-primary-700 dark:text-neutral-0 w-8 h-8" />
                    </a>
                  </Link>
                </div>
                <Image
                  src="/images/register.svg"
                  width={650}
                  height={650}
                  alt="women filling out a form"
                />
                <h2 className="text-primary-600 dark:text-neutral-0 font-bold header2 text-center w-4/6">
                  Start Learning Now
                </h2>

                <div className="absolute -bottom-20 -left-20 text-primary-600 opacity-20 dark:opacity-40">
                  <AbstractDotPattern />
                </div>
                <div className="absolute -top-36 -right-36 text-primary-600 opacity-20 dark:opacity-40">
                  <AbstractCirclePattern />
                </div>
              </div>
            </div>

            <div
              className={`flexable justify-center items-center ${styles['overlay-panel']} ${styles['overlay-right']}`}
            >
              <div className="relative flexable justify-center items-center h-full w-full">
                <div className="absolute top-6 left-[52%]">
                  <Link href="/">
                    <a className=" text-primary-700 dark:text-primary-200">
                      <HomeIcon className="text-primary-700 dark:text-neutral-0 w-8 h-8" />
                    </a>
                  </Link>
                </div>
                <Image
                  src="/images/login.svg"
                  width={720}
                  height={504}
                  alt="women filling out a form"
                />
                <h2 className="text-primary-600 dark:text-neutral-0 font-bold header2 text-center w-4/6">
                  Welcome back to Infotition
                </h2>
                <div className="absolute -bottom-20 -left-20 text-primary-600 opacity-20 dark:opacity-40">
                  <AbstractDotPattern />
                </div>
                <div className="absolute -top-36 -right-36 text-primary-600 opacity-20 dark:opacity-40">
                  <AbstractCirclePattern />
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </Container>
  );
};

export default Login;
