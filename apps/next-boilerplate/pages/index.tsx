import HomeTemplate from '@Templates/HomeTemplate';
import Title from '@Helpers/Title';

const Homepage = () => {
  return (
    <>
      <Title title={'Infotition'} />
      <HomeTemplate />
    </>
  );
};

export default Homepage;
