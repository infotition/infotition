import { useState } from 'react';

import BellIcon from '@heroicons/react/solid/esm/BellIcon';
import ChevronDownIcon from '@heroicons/react/solid/esm/ChevronDownIcon';
import MoonIcon from '@heroicons/react/solid/esm/MoonIcon';

import Title from '@Helpers/Title';
import Auth from '@Modules/AuthForm';
import Container from '@Layouts/Container';
import ThemeSwitch from '@Elements/ThemeSwitch';
import Navbar from '@Modules/Navbar';
import Brand from '@Elements/Brand';
import IconButton from '@Elements/IconButton';
import Dropdown from '@Modules/Dropdown';

const Debug = () => {
  const [active, setActive] = useState(false);

  return (
    <>
      <Title title={'Debug'} />

      <Container>
        <Navbar>
          <Brand redirect="/" />

          <Navbar.Section isList>
            <Navbar.Item>
              <IconButton ButtonIcon={BellIcon} label="Notifications" />
            </Navbar.Item>

            <Navbar.Item>
              <IconButton
                ButtonIcon={ChevronDownIcon}
                label="Account"
                onClick={() => setActive(!active)}
              />
              <Dropdown>
                <Dropdown.Menu active={active}>
                  <Dropdown.Item>
                    <ThemeSwitch position="bottom_right" />
                  </Dropdown.Item>
                </Dropdown.Menu>

                <Dropdown.Menu active={!active} isSecondary>
                  <div>Dropdown 2</div>
                  <div>Hello World</div>
                </Dropdown.Menu>
              </Dropdown>
            </Navbar.Item>
          </Navbar.Section>
        </Navbar>

        <Container.Center>
          <Auth className="tablet:w-1/2">
            <Auth.SignUp />
          </Auth>
        </Container.Center>
      </Container>
    </>
  );
};

export async function getStaticProps({ locale }) {
  const { default: lngDict = {} } = await import(
    `../public/locales/${locale}.json`
  );

  return { props: { lngDict } };
}

export default Debug;
