import ThemeSwitch from '@Elements/ThemeSwitch';
import type { NextPage } from 'next';

import Title from '../app/Components/Helpers/Title';
import Container from '../app/Components/Layouts/Container';

const Index: NextPage = () => {
  return (
    <>
      <Title title="Lightweight Next Boilerplate" />

      <ThemeSwitch position="upper_right" />

      <Container>
        <h1 className="text-header2m tablet:text-header2 text-center">
          Lightweight Next Boilerplate
        </h1>
      </Container>
    </>
  );
};

export default Index;
