import { FunctionComponent } from 'react';

/**
 * Container component with min screen height.
 */
const Container: FunctionComponent = ({ children }) => {
  return (
    <div className="bg-neutral-0 dark:bg-neutral-800 text-neutral-800 dark:text-neutral-0 min-h-screen flex flex-row mx-auto justify-center items-center space-y-5">
      {children}
    </div>
  );
};

export default Container;
