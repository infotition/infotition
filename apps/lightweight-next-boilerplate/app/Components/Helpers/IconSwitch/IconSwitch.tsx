import { FunctionComponent } from 'react';

import classNames from '@Utils/classNames';

import type { IconSwitchProps } from './IconSwitch.types';

/**
 * Generates a button which switches between the two provided items if
 * clicked with an animation.
 *
 * @param className   ('')    The styling which should be applied to the component.
 * @param onClick     (null)  The action which should be emitted if the icons got clicked.
 * @param toggle      (false) If the first icon or the second icon should be displayed.
 * @param FirstIcon           The first icon to gets rendered.
 * @param SecondIcon          The second icon to gets rendered.
 *
 * @example <IconSwitch toggle={isDarkTheme} FirstIcon={SunIcon} SecondIcon={MoonIcon} />
 */
const IconSwitch: FunctionComponent<IconSwitchProps> = ({
  ariaLabel = '',
  className = '',
  onClick = null,
  toggle = false,
  FirstIcon,
  SecondIcon,
}) => {
  return (
    <button
      aria-label={ariaLabel}
      aria-pressed={toggle}
      type="button"
      onClick={onClick}
      className={className}
    >
      <span className={`stack items-center`}>
        <FirstIcon
          className={classNames(
            'switch-icon stack-item',
            toggle && 'rotate-0 opacity-1',
            !toggle && 'rotate-180 opacity-0'
          )}
        />
        <SecondIcon
          className={classNames(
            'switch-icon stack-item',
            toggle && '-rotate-180 opacity-0',
            !toggle && 'rotate-0 opacity-1'
          )}
        />
      </span>
    </button>
  );
};

export default IconSwitch;
