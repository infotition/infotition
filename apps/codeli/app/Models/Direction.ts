/**
 * Type representing the directions an entity could possible move.
 */
type Direction = 'up' | 'down' | 'left' | 'right';

export default Direction;
