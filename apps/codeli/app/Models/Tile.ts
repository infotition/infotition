import Collision from './Collision';

/**
 * Every tile which can get placed on the field must implement this interface.
 * The interface takes care, that every tile defines the behaviour with a collision
 * with an entity and how it should gets rendered on the field.
 */
interface Tile {
  /**
   * Returns a specific collision object which defines the behaviour
   * after an entity collided with the tile.
   */
  onCollide(): Collision;

  /**
   * Returns a svg element which gets rendered by react if the tile
   * is somewhere on the field.
   */
  render(): JSX.Element;
}

export default Tile;
