/**
 * Type to encapsulate information based on a collision. An object of type collision
 * contains for example information about the solid or visible state of the collided tile.
 */
type Collision = {
  isSolid: boolean;
  isVisible: boolean;
  damage: number;
};

export default Collision;
