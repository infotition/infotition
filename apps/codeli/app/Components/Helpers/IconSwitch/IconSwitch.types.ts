/* eslint-disable @typescript-eslint/no-explicit-any */

import { FunctionComponent } from 'react';

export type IconSwitchProps = {
  ariaLabel?: string;
  className?: string;
  onClick?: VoidFunction;
  toggle?: boolean;
  FirstIcon: FunctionComponent<any>;
  SecondIcon: FunctionComponent<any>;
};
