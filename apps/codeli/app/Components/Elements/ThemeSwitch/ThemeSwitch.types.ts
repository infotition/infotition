export type ThemeSwitchProps = {
  className?: string;
  position?: 'upper_right' | 'upper_left' | 'bottom_right' | 'bottom_left';
};
