import SunIcon from '@heroicons/react/solid/esm/SunIcon';
import MoonIcon from '@heroicons/react/solid/esm/MoonIcon';

import { useTheme } from 'next-themes';
import { FunctionComponent } from 'react';

import IconSwitch from '@Helpers/IconSwitch';
import useIsMounted from '@Hooks/useIsMounted';
import ClientOnlyPortal from '@Helpers/ClientOnlyPortal';
import classNames from '@Utils/classNames';

import type { ThemeSwitchProps } from './ThemeSwitch.types';
import { switchPositionMap } from './ThemeSwitch.constants';

/**
 * Generates a sun/moon icon button which changes the theme from dark to light
 * and vice versa if pressed.
 *
 * @param className ('')             The styling which should be applied to the component.
 * @param position ('upper_right')  The position of the theme switch.
 *
 * @example <ThemeSwitch position="upper_right" />
 */
const ThemeSwitch: FunctionComponent<ThemeSwitchProps> = ({
  className = '',
  position = 'upper_right',
}) => {
  const { theme, setTheme, resolvedTheme } = useTheme();
  const mounted = useIsMounted();

  const isDarkTheme = mounted && (theme === 'dark' || resolvedTheme === 'dark');

  /**
   * Sets the theme to dark, if its currently in like and vice versa.
   */
  const handleToggleTheme = () => setTheme(isDarkTheme ? 'light' : 'dark');

  /**
   * Returns the tailwind absolute position classes for the provided
   * theme switch position prop.
   */
  const getPosition = () => switchPositionMap.get(position);

  return (
    mounted && (
      <ClientOnlyPortal selector="#theme-switch">
        <IconSwitch
          ariaLabel={'toggle_theme'}
          onClick={handleToggleTheme}
          toggle={isDarkTheme}
          FirstIcon={SunIcon}
          SecondIcon={MoonIcon}
          className={classNames(
            className,
            'fixed z-50 text-neutral-800 dark:text-neutral-0 w-9 h-9',
            getPosition()
          )}
        />
      </ClientOnlyPortal>
    )
  );
};

export default ThemeSwitch;
