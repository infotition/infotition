import Direction from '@Models/Direction';
import Vector from '@Utils/Vector';

/**
 * Class to define the behaviour every entity of the field should inherit from.
 * This means simple functionality like create and move.
 */
class Entity {
  /* This map defines the direction a entity should move based on a given direction. */
  private static offsetMap = new Map([
    ['down', new Vector(0, 1)],
    ['up', new Vector(0, -1)],
    ['left', new Vector(-1, 0)],
    ['right', new Vector(1, 0)],
  ]);

  /* The position of the entity on the field. */
  private position: Vector;

  /* The reference to the field the entity is on. */
  //private field: Field;

  /**
   * Get the position of the entity.
   *
   * @returns The position of the entity.
   */
  public getPosition(): Vector {
    return this.position;
  }

  public move(direction: Direction): boolean {
    const moveDestination = this.position
      .copy()
      .add(this.getOffsetFromDirection(direction));

    //const collision = this.field.checkCollision(moveDestination);

    return true;
  }

  /**
   *
   * @param direction The direction the entity wants to move.
   * @returns         The offset which should get added to the pos vector to move.
   */
  private getOffsetFromDirection(direction: Direction): Vector {
    return Entity.offsetMap.get(direction);
  }
}

export default Entity;
