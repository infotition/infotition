import Collision from '@Models/Collision';

/**
 * Builder Class to create collisions based of various tiles of the map.
 *
 * Default values for the collision object:
 * - isSolid = false
 * - isVisible = false
 * - damage = 0
 */
class CollisionBuilder {
  private readonly _collision: Collision;

  /**
   * Creates a new instance of a collision and initializes the collision
   * with default value, so they can be added later.
   */
  public constructor() {
    this._collision = { isSolid: false, isVisible: false, damage: 0 };
  }

  /**
   * Assigns a solid state to the collision object.
   *
   * @param isSolid   Whether the tile is solid or not.
   * @returns         The reference to the collision builder.
   */
  public isSolid(isSolid = true): CollisionBuilder {
    this._collision.isSolid = isSolid;
    return this;
  }

  /**
   * Assigns a visible state to the collision object.
   *
   * @param isVisible   Whether the tile is visible on the field or not.
   * @returns           The reference to the collision builder.
   */
  public isVisible(isVisible = true): CollisionBuilder {
    this._collision.isVisible = isVisible;
    return this;
  }

  /**
   * Assigns the damage the entity should take after colliding with
   * the tile to the collision object.
   *
   * @param damage   The damage the entity should take after colliding.
   * @returns        The reference to the collision builder.
   */
  public damage(damage: number): CollisionBuilder {
    this._collision.damage = damage;
    return this;
  }

  /**
   * Returns the built collision object with the assigned values.
   *
   * @returns The collision object.
   */
  public build(): Collision {
    return this._collision;
  }
}

export default CollisionBuilder;
