import Tile from '@Models/Tile';
import CollisionBuilder from '@Controllers/CollisionBuilder';

class EmptyTile implements Tile {
  onCollide = () => new CollisionBuilder().build();

  render = () => <rect width="30" height="30" fill="blue" />;
}

class PlayerTile implements Tile {
  onCollide = () => new CollisionBuilder().isSolid().build();

  render = () => <rect width="30" height="30" fill="red" />;
}

class RockTile implements Tile {
  onCollide = () => new CollisionBuilder().isSolid().damage(2).build();

  render = () => <rect width="30" height="30" fill="green" />;
}

const DefaultTiles = [typeof EmptyTile, typeof PlayerTile, typeof RockTile];

export default DefaultTiles;
