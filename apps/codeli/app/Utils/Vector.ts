/**
 * Utility class to wrap two 2D coordinates inside a object. There
 * are also useful methods, which can be executed with vectors
 * like add, subtract or multiply.
 */
class Vector {
  /* The x coordinate of the vector. */
  public x: number;

  /* The y coordinate of the vector. */
  public y: number;

  /**
   * Creates an instance of the vector with the given
   * two coordinates / values.
   *
   * @param x   The x coordinate of the vector.
   * @param y   The y coordinate of the vector.
   */
  public constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  /**
   * Create a new vector with the same x and y values as this vector.
   * In other words, it creates an copy of the vector.
   *
   * @returns A copy of the vector.
   */
  public copy(): Vector {
    return new Vector(this.x, this.y);
  }

  /**
   * Adds the given vector to this vector.
   *
   * @param offset  The second vector which should get added to this vector.
   * @returns       The reference to the current vector.
   */
  public add(offset: Vector): Vector {
    this.x += offset.x;
    this.y += offset.y;
    return this;
  }

  /**
   * Subtract the given vector from this vector.
   *
   * @param offset  The second vector which should get subtracted to this vector.
   * @returns       The reference to the current vector.
   */
  public subtract(offset: Vector): Vector {
    this.x -= offset.x;
    this.y -= offset.y;
    return this;
  }

  /**
   * Multiply the given vector with a scalar.
   *
   * @param scalar  The scalar which should get multiplied to the coordinates.
   * @returns       The reference to the current vector.
   */
  public multiply(scalar: number): Vector {
    this.x *= scalar;
    this.y *= scalar;
    return this;
  }
}

export default Vector;
