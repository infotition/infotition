/**
 * Object which contains three helper methods to access the standard javascript
 * event bus within react. Can be used to send data to different components, classes
 * and so on.
 */
const eventBus = {
  /**
   * Listen for an event on the html document which calls the callback
   * function after it got dispatched from somewhere else.
   *
   * @typeParam T     The type of the expected payload.
   * @param event     The unique identifier of the event.
   * @param callback  The callback which gets invoked after the event got dispatched.
   */
  on<T>(event: string, callback: (e: CustomEvent<T>) => void) {
    document.addEventListener(event, callback);
  },

  /**
   * Dispatch an event so that all listeners receive the payload
   * object.
   *
   * @typeParam T     The type of the payload which should get dispatched.
   * @param event     The unique identifier of the event.
   * @param data      The data payload which should get dispatched.
   */
  dispatch<T>(event: string, data: T) {
    document.dispatchEvent(new CustomEvent(event, { detail: data }));
  },

  /**
   * Removes a callback to listen for a specific event. **IMPORTANT**: The callback
   * parameter must be the exact same function, as the one which started listening to.
   * Two anonymous functions for example will not work, even if they to the exact same.
   * They are still two separate functions.
   *
   * @typeParam T     The type of the payload.
   * @param event     The unique identifier of the event.
   * @param callback  The callback which gets invoked after the event got dispatched.
   */
  remove<T>(event: string, callback: (e: CustomEvent<T>) => void) {
    document.removeEventListener(event, callback);
  },
};

export default eventBus;
