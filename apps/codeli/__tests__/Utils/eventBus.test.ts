import eventBus from '@Utils/eventBus';

type TestObject = {
  test: number;
};

it('should listen for dispatched events', () => {
  const testCallback = ({ detail }: CustomEvent<TestObject>) => {
    expect(detail).toEqual({ test: 42 });
  };

  eventBus.on<TestObject>('test', testCallback);
  eventBus.dispatch<TestObject>('test', { test: 42 });

  eventBus.remove<TestObject>('test', testCallback);
});

it('should correctly remove event listeners', () => {
  // If the callback gets called, it invokes an invalid assertion
  const testCallback = () => expect(true).toBe(false);

  // Add the callback, remove it instantly and try to dispatch it
  // to see if removing the callback worked
  eventBus.on<TestObject>('test', testCallback);
  eventBus.remove<TestObject>('test', testCallback);
  eventBus.dispatch<TestObject>('test', { test: 42 });
});
