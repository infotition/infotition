import Vector from '@Utils/Vector';

it('should create a deep copy of the vector', () => {
  // Creating a fresh vector and checking the values
  const vector = new Vector(5, 6);
  expect({ x: vector.x, y: vector.y }).toEqual({ x: 5, y: 6 });

  // Make a copy of the vector and change the original one
  const copy = vector.copy();
  vector.x = 11;

  // The original one should reflect the changes, but not the copy
  // because it should create a new reference.
  expect({ x: vector.x, y: vector.y }).toEqual({ x: 11, y: 6 });
  expect({ x: copy.x, y: copy.y }).toEqual({ x: 5, y: 6 });
});

it('should correctly add a vector', () => {
  const vector = new Vector(5, 6);
  vector.add(new Vector(3, 5));
  expect({ x: vector.x, y: vector.y }).toEqual({ x: 8, y: 11 });
});

it('should correctly subtract a vector', () => {
  const vector = new Vector(5, 6);
  vector.subtract(new Vector(3, 5));
  expect({ x: vector.x, y: vector.y }).toEqual({ x: 2, y: 1 });
});

it('should correctly multiply a vector with scalar', () => {
  const vector = new Vector(5, 6);
  vector.multiply(5);
  expect({ x: vector.x, y: vector.y }).toEqual({ x: 25, y: 30 });
});
