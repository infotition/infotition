import CollisionBuilder from '@Controllers/CollisionBuilder';

it('should create an non existing tile as default', () => {
  expect(new CollisionBuilder().build()).toEqual({
    isSolid: false,
    isVisible: false,
    damage: 0,
  });
});

it('should create adjust the values with the build functions', () => {
  expect(
    new CollisionBuilder().damage(100).isSolid(true).isVisible(false).build()
  ).toEqual({
    isSolid: true,
    isVisible: false,
    damage: 100,
  });
});

it('should be able to enable booleans without true parameter', () => {
  expect(new CollisionBuilder().isSolid().isVisible().build()).toEqual({
    isSolid: true,
    isVisible: true,
    damage: 0,
  });
});
