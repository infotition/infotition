import ThemeSwitch from '@Elements/ThemeSwitch';
import Direction from '@Models/Direction';
import Vector from '@Utils/Vector';
import type { NextPage } from 'next';
import { useEffect, useState } from 'react';
import Unity, { UnityContext } from 'react-unity-webgl';

import Title from '../app/Components/Helpers/Title';
import Container from '../app/Components/Layouts/Container';

const unityContext = new UnityContext({
  loaderUrl: 'https://d2gel7vkj4fxg4.cloudfront.net/codeli/build.loader.js',
  dataUrl: 'https://d2gel7vkj4fxg4.cloudfront.net/codeli/build.data.br',
  frameworkUrl:
    'https://d2gel7vkj4fxg4.cloudfront.net/codeli/build.framework.js.br',
  codeUrl: 'https://d2gel7vkj4fxg4.cloudfront.net/codeli/build.wasm.br',
});

const offsetMap = new Map([
  ['down', new Vector(0, -1)],
  ['up', new Vector(0, 1)],
  ['left', new Vector(-1, 0)],
  ['right', new Vector(1, 0)],
]);

const Index: NextPage = () => {
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);

    unityContext.on('debug', function (message) {
      console.log(message);
    });
  }, []);

  const move = (direction: Direction) => {
    unityContext.send('Cat', 'Move', JSON.stringify(offsetMap.get(direction)));
  };

  return (
    <>
      <Title title="Codeli" />

      <ThemeSwitch position="upper_right" />

      <Container className="flex-col">
        {mounted && <Unity unityContext={unityContext} className="w-full" />}
        <div className="text-body1">
          <div className="flex justify-center">
            <button onClick={() => move('up')}>⬆️</button>
          </div>
          <div>
            <button onClick={() => move('left')}>⬅️</button>
            <button onClick={() => move('down')}>⬇️</button>
            <button onClick={() => move('right')}>➡️</button>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Index;
