import type { AppProps } from 'next/app';
import { ThemeProvider } from 'next-themes';

import '../app/Styles/base.scss';

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider attribute="class" enableSystem={false}>
      <Component {...pageProps} />
    </ThemeProvider>
  );
}

export default CustomApp;
