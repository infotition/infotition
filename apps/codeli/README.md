<div align="center">
	<br />
	<p>
		<a href="http://infotition.de">
			<img src="../../.gitlab/assets/infotition_logo.png" width=600px alt="infotition logo" />
		</a>
	</p>
	<h1>Infotition's lightweight Next.js Boilerplate</h1>
	<p>This Boilerplate provides full support for various development features like Typescript, TailwindCSS, SCSS, Jest, Prettier, Eslint and Stylelint.</p>
</div>

## Warnings
This Boilerplate is **not production ready** for it's own. It doesn't really configure security headers, sso and much more. This boilerplate is used for **development only** eg. if developing some modules of a bigger project. If you are interested in a production ready one, check out the normal infotition next boilerplate.

# Table of Contents

- [Table of Contents](#table-of-contents)
- [About](#about)
- [Installation](#installation)
- [Features](#features)
- [Deployment](#deployment)
- [Issue Reporting](#issue-reporting)
- [Contribution](#contribution)
- [License](#license)


# About

Next.js is a minimalistic React framework that runs on the browser and the server which calls itself "The React Framework for Production". It offers the best developer experience with all the features you need for production: hybrid static & server rendering, TypeScript support, smart bundling, route pre-fetching, and more. It's also a straightforward way for developers with React experience to get productive quickly.

This lightweight boilerplate makes it easier to get up and running with a well-structured Next.js and TypeScript application to develop submodules of your app without any unneeded production configurations.

**IMPORTANT**: This Boilerplate is **not production ready** for it's own. It doesn't really configure security headers, sso and much more. This boilerplate is used for **development only** eg. if developing some modules of a bigger project. If you are interested in a production ready one, check out the normal infotition next boilerplate.


# Installation

Next.js requires [Node.js 12.0]([nodejs.org/](ttps://nodejs.org/en/download/)) or later as system requirement. This Project uses [yarn](https://yarnpkg.com/) as it's package manager. Make sure you have installed both requirements.

Clone the repository and change the directory of your terminal to the downloaded folder.
```bash
$ git clone https://gitlab.com/infotition/infotition.git
$ cd infotition/apps/lightweight-next-boilerplate
```

Install all development and production dependencies.
```bash
$ yarn install
```

Now everything should be working fine. You can run the development server with `yarn run dev`. If you have any problems, don't hesitate to join our official [discord server](https://discord.gg/NpxrDGYDwV).

# Features

This Boilerplate project provides a lot of features out of the box. Here's an overview of the included components and tools.

- **Next.js** - Minimalistic framework for server-rendered React applications.
- **Typescript** - Superset of JavaScript which primarily provides optional static typing, classes and interfaces.
- **Sass/Scss** - CSS preprocessor, which adds special features such as variables, nested rules and mixins (sometimes referred to as syntactic sugar) into regular CSS.
- **Docker** - A tool designed to make it easier to create, deploy, and run applications by using containers.
- **ESLint** - The pluggable linting utility.
- **Prettier** - An opinionated code formatter.
- **Stylelint** - A mighty, modern linter that helps you avoid errors and enforce conventions in your styles.
- **Bundler Analyzer** - Visualize the size of webpack output files with an interactive zoomable treemap.
- **Jest** - Javascript testing framework, created by developers who created React.
- **React Testing Library** - Simple and complete React DOM testing utilities that encourage good testing practices.
- **TailwindCSS** - A utility-first CSS framework that can be composed to build any design, directly in your markup.
- **Next Themes** - An abstraction for themes in your Next.js app. An Theme Switcher Component as React Portal is also included and supports tailwind dark pseudo classes.

# Deployment

Infotition is a giant monorepo which uses [Turborepo](https://turborepo.org/) as its build system. If you want to deploy a application, you can use the provided Dockerfiles.

If you want to deploy to [Vercel](https://vercel.com) you can follow the [official guidelines](https://vercel.com/docs/concepts/git/monorepos) to handle monorepos (especially turborepo) with Vercel.

# Issue Reporting

If you have found a bug or if you have a feature request, please report them at this repository issues section. For other related questions/support please use the official Infotition [Discord server](https://discord.gg/NpxrDGYDwV).

# Contribution

We appreciate feedback and contribution to this repo! Before you get started, please see the following:

- [Infotition Code of Conduct guidelines](https://gitlab.com/infotition/infotition/-/blob/main/.gitlab/CODE_OF_CONDUCT.md)
- [Infotition Contribution guidelines](https://gitlab.com/infotition/infotition/-/blob/main/.gitlab/CONTRIBUTING.md)

# License

This repo is covered under the MIT License, see the [LICENSE](https://gitlab.com/infotition/infotition/-/blob/main/LICENSE) file for more information.
